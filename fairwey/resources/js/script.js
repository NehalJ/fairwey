function stopScrollify(){

	  $.scrollify.disable();
}
function startSrollify(){
	
	$.scrollify.enable();
}
$(document).ready(function() {
     $('.aniview').AniView();
     $(".button-collapse").sideNav();
     	
     	// get current URL path and assign 'active' class
		var pathname = window.location.pathname;
		//split url to / for last part of url
		var url=pathname.split("/").pop();
		// remove current active class from li
		$("nav li.active").removeClass("active");
		//add url wise active class to li
		$('nav ul li>a[href="'+url+'"]').parent().addClass('active');
		
		$('.modal').modal({
    	// Callback for Modal open. Modal and trigger parameters available.
        	 ready: function(modal, trigger) { 
    	        
    	        stopScrollify();
    	      },
    	    
    	   // Callback for Modal close
    	 complete: function() {startSrollify();
    	 }
     });
     $('select').material_select();
     $('ul.tabs').tabs();
     $('.slider').slider();
     $(".leftclick").click(function() {
         $('.slider').slider('prev');
     });
     $(".rightclick").click(function() {
         $('.slider').slider('next');
     });
     
     
     /*$(;"#addUser").click(function(e) {
         var a = $("#selectService").val();
         if (a === undefined) {
             alert("Please Select atleast one type of service");
             e.preventDefault();
         }
         if (a == 0 ) {
             alert("Please unselect choose your option and select atleast one type of service");
             e.preventDefault();
         }

     });*/
 });
 window.onscroll = function() {
     scrollFunction()
 };

 function scrollFunction() {
     if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
         document.getElementById("myBtn").style.display = "block";
     } else {
         document.getElementById("myBtn").style.display = "none";
     }
 }
 // When the user clicks on the button, scroll to the top of the document
 function topFunction() {
     document.body.scrollTop = 0; // For Chrome, Safari and Opera 
     document.documentElement.scrollTop = 0; // For IE and Firefox
 }