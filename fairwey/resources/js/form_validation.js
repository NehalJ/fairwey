// Wait for the DOM to be ready
$(function() {
	jQuery.validator.addMethod(
	          "notEqualTo",
	          function(elementValue,element,param) {
	            return elementValue != param;
	          },
	          "Value cannot be {0}"
	        );
	
	
    // Initialize form validation on the registration form.
    // It has the name attribute "registration"
    $("form[name='contact']").validate({
        // Specify validation rules
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side
        	userDetailsName: "required",
        	mobileNumber: {
        		required: true,
                number: true,
                minlength: 10,
                maxlength: 10
                //pattern: [/^[7-9]{1}[0-9]{9}$/]

            },
            emailId: {
                required: true,
                // Specify that email should be validated
                // by the built-in "email" rule
                email: true
            },
            serviceId: "required",
            userDetailsMessage: "required"


        },
        // Specify validation error messages
        messages: {
        	userDetailsName: "Please enter your firstname",
        	mobileNumber: {

        		required: "Please provide a mobile number",
                number: "please enter only number",
                minlength: "mobile no should be 10 digit",
                maxlength: "mobile no should be 10 digit"
               // pattern: "please enter a valid mobile no"

            },

            emailId: "Please enter a valid email address",
            serviceId:"please select service",
            userDetailsMessage: "please enter a message"
        },

        errorElement: 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
       
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function(form) {
        	form.submit();
        	$('#addeditmsg').modal('open');	
			$('#msg').text("Submitted Succesfully");            
        }
    });
    
    
    $(function(){
 	   $("form[name='contactForm']").validate({
 	    	errorElement: 'div',
 	        errorPlacement: function(error, element) {
 	            var placement = $(element).data('error');
 	            if (placement) {
 	                $(placement).append(error)
 	            } else {
 	                error.insertAfter(element);
 	            }
 	        },
 	    
 	        // Specify validation rules
 	        rules: {
 	            // The key name on the left side is the name attribute
 	            // of an input field. Validation rules are defined
 	            // on the right side
 	        	userDetailsName: "required",
 	        	mobileNumber: {
 	        		required: true,
 	                number: true,
 	                minlength: 10,
 	                maxlength: 10
 	                //pattern: [/^[7-9]{1}[0-9]{9}$/]

 	            },
 	            emailId: {
 	                required: true,
 	                // Specify that email should be validated
 	                // by the built-in "email" rule
 	                email: true
 	            },
 	            serviceId: "required"
 	           
 	        },
 	        // Specify validation error messages
 	        messages: {
 	        	userDetailsName: "Please enter your name",
 	        	mobileNumber: {

 	        		required: "Please provide a mobile number",
 	                number: "please enter only number",
 	                minlength: "mobile no should be 10 digit",
 	                maxlength: "mobile no should be 10 digit"
 	               // pattern: "please enter a valid mobile no"

 	            },


 	            emailId: "Please enter a valid email address",
 	            serviceId:"please select service"

 	              },
 	              
 	        
 	        // Make sure the form is submitted to the destination defined
 	        // in the "action" attribute of the form when valid
 	        submitHandler: function(form) {
 	        	
 	        	form.submit();
 	        	Materialize.toast("Submitted Successfully..", 10000);
 	        }
 	    });
    });
 
    
});