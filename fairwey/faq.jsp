<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
     
    <%@include file="components/header_import.jsp"%>
    
    <style>
        .row {
            margin-left: 0;
            margin-right: 0;
        }
        
        .faq {
            opacity: 1;
            margin-top: -10%;
        }
        
        @media only screen and (max-width:600px) {
            .faq {
                opacity: 1;
                margin-top: -15%;
            }
        }
    </style>
    <script>
        $(document).ready(function() {
        	$(".dropdown-content li").click(function(){	 
                var a= $(".dropdown-content li").hasClass('active');
         		if(a === true){
         			
         			$("#selectService-error").css("display","none");
         		}
         		else if(a === false)
         				 {
         			$("#selectService-error").css("display","block");
         				 }
         		 
         	 });
        });
    </script>
</head>
<body>

	<!--navbar start-->
		<%@include file="components/navbar.jsp"%>
	<!--navbar end-->

    
    
    <div class="row">
        <div class="col s12 m12 l12" style="padding:0;">
            <img src="resources/image/faq_crop.jpg" alt="FAQ" class="responsive-img">

            <h3 class="white-text  caption center-align faq">FAQ</h3>

        </div>

    </div>
    <div class="row">
        <br><br> <br><br>
        <div class="col s12 m12 l8 offset-l2">
            <ul class="collapsible popout" data-collapsible="accordion">
                <li>
                    <div class="collapsible-header"><i class="material-icons">add</i>What do you expect?</div>
                    <div class="collapsible-body"><span><b>Ans:</b> All your finances are taken care of under one roof.</span></div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">add</i>What is the basis for fixing the pricing of credit facilities?</div>
                    <div class="collapsible-body"><span><b>Ans:</b> The pricing of the credit facilities will be based on the credit rating of the borrower carried out by the bank.</span></div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">add</i>Do you have solutions for CIBIL Credit Report Problems?</div>
                    <div class="collapsible-body"><span><b>Ans:</b> Yes, please feel free to contact us.</span></div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">add</i>Does your firm extend the corporate loans or Line of Credit for short-term working capital requirements?</div>
                    <div class="collapsible-body"><span><b>Ans:</b> Yes. We consider corporate loans or Line of credit for short-term working capital requirements.</span></div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">add</i>What is Credit evaluation and how it is evaluated by Fairwey Advisory?  </div>
                    <div class="collapsible-body"><span><b>Ans:</b> Credit evaluation and approval is the process a business or an individual must go through to become eligible for a loan or to pay for goods and services over an extended period. It also refers to the process businesses or lenders undertake when evaluating a request for credit.
Fairwey Advisory will examine the viability of our potential customer's business proposal based on capacity and commitment to pay while observing sound banking practices and standards as dictated by regulators and in spirit with our internal code of conduct.</span></div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">add</i>Why hire Fairwey Advisory?</div>
                    <div class="collapsible-body"><span style="font-size:13px;">Ans: Consumers in need of corporate Finance,Retail Finance,Rural Finance,Distribution and Broking,Wealth Management,Investment Banking and Private Equity, needs should hire Fairwey advisory because:<br>
                   
                  <i class="material-icons tiny prefix">fiber_manual_record</i>We offer comprehensive services</br>
 <i class="material-icons tiny prefix">fiber_manual_record</i> We have our clients' needs at heart and develop customized plans based on client's requirement, tolerances, and preferences</br>
<i class="material-icons tiny prefix">fiber_manual_record</i> We are accessible, and provide the tools and communication mechanisms to keep you informed of your case update on a regular basis<br>
<i class="material-icons tiny prefix">fiber_manual_record</i> Unlike advisors, who often just sell suitable products, we have a fiduciary duty to uphold.<br> </span></div>
                </li>
                 <li>
                    <div class="collapsible-header"><i class="material-icons">add</i>How is Fairwey advisory different from other financial advisory firms?</div>
                    <div class="collapsible-body"><span><b>Ans:</b> Fairwey advisory differs from the competition in several key areas, including our technical expertise, the market, our services and our delivery method.<br>
                    <i class="material-icons tiny prefix">fiber_manual_record</i> Fairwey advisory is an independent firm which operates on a fee-based basis. In comparison, to other advisors generally charge.<br>
                    <i class="material-icons tiny prefix">fiber_manual_record</i> Fairwey advisory believes in providing low fee products, when possible, to derive the greatest value.<br>
                    </span></div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">add</i>Is Fairwey advisory is looking for any specific client?</div>
                    <div class="collapsible-body"><span><b>Ans:</b> No, there is no specific requirement; we are open to everyone irrespective of sectors or locations.</span></div>
                </li>
                <li>
                    <div class="collapsible-header"><i class="material-icons">add</i>How does Fairwey advisory charge its clients?</div>
                    <div class="collapsible-body"><span><b>Ans:</b> Charges varies based on the service provided. Please feel free to contact us.</span></div>
                </li>
            </ul>
        </div>
        
    </div>
    <!--footer start-->

   			<%@include file="components/footer.jsp"%>

    <!--footer end-->
    <!--action button strat-->
    <div class="fixed-action-btn">
        <a class="btn-floating btn-large black">
            <i class="large material-icons">view_list</i>
        </a>
        <ul>
            <li><a class="btn-floating red tooltipped" data-position="left" data-delay="50" data-tooltip="Emi Calculator" href="emiPageForWebsite"><i class="material-icons">insert_chart</i></a></li>
            <li><a class="btn-floating yellow darken-1 tooltipped" data-position="left" data-delay="50" data-tooltip="Contact Us" href="contactPageForWebsite"><i class="material-icons">perm_contact_calendar</i></a></li>
            <li><a class="btn-floating green modal-trigger tooltipped" data-position="left" data-delay="50" data-tooltip="Call Me" href="#contactForm"><i class="material-icons">phone</i></a></li>

        </ul>
    </div>


    <!-- Modal Structure -->
    <div id="contactForm" class="modal modal-fixed-footer grey lighten-3">
     <form method="post" action="addingUserDetailsForWebsiteFaqPageModal" id="addUserForm" name="contactForm">
        <div class="row modal-content">
            <h4 class="center">Let Us Contact You</h4>
            <hr>
            
                <div class="input-field col s12 m6 l6 offset-l3 offset-m3">
                        <i class="material-icons prefix">people</i>
                        <input id="userDetailsName" name="userDetailsName" type="text" class="validate text" required>
                        <label for="userDetailsName">Name</label>
                    </div>
                    
                    <div class="input-field col s12 m6 l6 offset-l3 offset-m3">
                    <i class="material-icons prefix">email</i>
                    <input id="emailId" name="emailId" type="email" class="validate" required>
                    <label for="emailId">Email</label>
                </div> 
                    
                <div class="input-field col s12 m6 l6 offset-l3 offset-m3">
                    <i class="material-icons prefix">phone</i>
                    <input id="mobileNumber" name="mobileNumber" type="text" class="validate num" required>
                    <label for="mobileNumber">Phone</label>
                </div>
                <div class="input-field col s12 m6 l6 offset-l3 offset-m3">
                    <i class="material-icons prefix">redeem</i>
                    
                    <select multiple name="serviceId" id="selectService" required aria-required="true">
						<option value="" disabled>Choose your option</option>
						
						<c:if test="${not empty serviceList}">
						<c:forEach var="service" items="${serviceList}">
						
							<option value="<c:out value="${service.serviceId}" />"><c:out
									value="${service.serviceName}" /></option>
						</c:forEach>
						</c:if>
                         
                    </select>
                    <label>Select Services Type</label>
                </div>

        </div>

        <div class="row modal-footer grey lighten-3">
            <div class="col s6 m6 l6 offset-m1 offset-l1 offset-s3">
                <button type="submit" name="action" id="addUser" href="#!" class="modal-action  btn waves-effect black">Submit</button>
            </div>
        </div>
        </form>
    </div>


    <!--action button end-->

    <div onclick="topFunction()" id="myBtn"><img src="resources/image/arrow-up-black.png" alt="scroll up" /></div>
</body>
</html>