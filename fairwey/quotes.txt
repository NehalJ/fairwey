
Warren Buffett 

-----Price is what you pay. Value is what you get.

-----The best preparation for tomorrow is doing your best today.

-----�Rule No. 1: Never lose money. Rule No. 2: Never forget rule No.1�
-----�It�s far better to buy a wonderful company at a fair price, than a fair company at a wonderful price.�
-----�Only buy something that you�d be perfectly happy to hold if the market shut down for 10 years.�
-----�Someone�s sitting in the shade today because someone planted a tree a long time ago.�
-----�An investor should act as though he had a lifetime decision card with just twenty punches on it.�

------�If you aren�t thinking about owning a stock for 10 years, don�t even think about owning it for 10 minutes.�

------Do not dwell in the past, do not dream of the future, concentrate the mind on the present moment.


------�Never assume that all is well when it comes to your savings or loans. Get a printout to ensure you know exactly what is happening.� 

-----�The man who never has money enough to pay his debts has too much of something else.�

------�The only man who sticks closer to you in adversity than a friend is a creditor.�
--------A good financial plan is a road map that shows us exactly how the choices we make today will affect our future.
---------Manage your spending by creating and sticking to a budget.
----------When you have a lot of money, there's so many places you can go to manage your money. But when you don't have money, mathematically you actually need a financial plan more. You can't really afford to make mistakes. So why is this such a luxury product?


Acquisition finance :-The type of funding obtained by a business for the purpose of purchasing another business.


-----�Promises make debt, and debt makes promises.�

-----�No man�s credit is as good as his money.�
------�Neither a borrower nor a lender be; for loan doth oft lose both itself and friend.�

--------�You can have everything in life you want, if you will just help other people get what they want.�



