<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	
	<%@include file="components/header_import.jsp"%>
	
<style>
        .row {
            margin-left: 0;
            margin-right: 0;
        }
        .input-field div.error {
            position: relative;
            top: -1rem;
            left: 15%;
            font-size: 0.8rem;
            color: #FF4081;
            -webkit-transform: translateY(0%);
            -ms-transform: translateY(0%);
            -o-transform: translateY(0%);
            transform: translateY(0%);
        }
    </style>
<script>
        $(function() {
            if ($(window).width() >= 770) {
                $.scrollify({
                    section: ".sectionDiv",
                    sectionName: ".sectionDiv",
                    easing: "easeOutExpo",
                    scrollSpeed: 1100,
                    offset: 0,
                    scrollbars: true,
                    before: function() {},
                    after: function() {},
                    afterResize: function() {}
                });
            }
        });
        $(document).ready(function() {
            $('.slider').slider();
            $(".leftclick").click(function() {
                $('.slider').slider('prev');
            });
            $(".rightclick").click(function() {
                $('.slider').slider('next');
            });
            $(".button-collapse").sideNav();
            $('#partners .carousel').carousel({

                duration: 300,
                padding: 30,
                //  use this to increase distance between images
                dist: 0
            });
            autoplayPartners();

            function autoplayPartners() {
                $('#partners .carousel').carousel('next');
                setTimeout(autoplay, 5000);

            };

            $('#prevButton1').click(function() {
                $('#partners .carousel').carousel('prev');
            });
            $('#nextButton1').click(function() {
                $('#partners .carousel').carousel('next');
            });
            $('#prevBtn1').click(function() {
                $('#partners .carousel').carousel('prev');
            });
            $('#nextBtn1').click(function() {
                $('#partners .carousel').carousel('next');
            });
            $('.carousel').carousel({
                duration: 100,
                padding: 30,
                //  use this to increase distance between images
                dist: 0
            });
            autoplay();

            function autoplay() {
                $('.carousel').carousel('next');
                setTimeout(autoplay, 5000);

            };
            $('#prevButton').click(function() {
                $('#testimonial .carousel').carousel('prev');
            });
            $('#nextButton').click(function() {
                $('#testimonial .carousel').carousel('next');
            });
            $('#prevBtn').click(function() {
                $('#testimonial .carousel').carousel('prev');
            });
            $('#nextBtn').click(function() {
                $('#testimonial .carousel').carousel('next');
            });

            $('.parallax').parallax();
            $(".dropdown-content li").click(function(){	 
                var a= $(".dropdown-content li").hasClass('active');
         		if(a === true){
         			
         			$("#selectService-error").css("display","none");
         		}
         		else if(a === false)
         				 {
         			$("#selectService-error").css("display","block");
         				 }
         		 
         	 });
         
        });
    </script>
</head>
<body>
	<!--navbar start-->
		<%@include file="components/navbar.jsp"%>
	<!--navbar end-->
	
	
	
	
	<!-- content start -->
	
	  <div class="row sectionDiv">
        <div class="col s12 m12 l10 offset-l1">
            <br><br><br>
            <h3 class="center" style="margin-bottom:0">Our Clients</h3>
            <!--<img src="resources/image/clientspic.png" alt="" class="responsive-img">-->
        </div>
        <div class="col s12 m12 l10 offset-l1 aniview hide-on-med-and-down" data-av-animation="slideInLeft">
            <img src="resources/image/clientspic1.png" alt="" height="520px;" style="margin-left:7%">
        </div>
        <div class="col s12 m12 l10 offset-l1 aniview hide-on-large-only" data-av-animation="slideInLeft">
            <img src="resources/image/clientspic1.png" alt="" class="responsive-img">
        </div>
    </div>
    
    
    
    
			<!--testimonial start-->
        <div id="testimonialDiv" class="aniview sectionDiv " data-av-animation="slideInUp">
            <div class="row hide-on-med-and-down">
                <br><br><br><br><br>
                <h3 class="center">Testimonial</h3>
                <!-- carousel for large device starts -->
                <div class=" grey lighten-4 z-depth-1 aniview" data-av-animation="slideInUp" style="margin-top:5%;margin-bottom:5%;">

                    <div class="col l1 m6 s6">
                        <a id="prevButton" class="waves-effect waves-light" style="transform: translate(0,140px)">
                            <i class="large material-icons black-text grey lighten-2">navigate_before</i></a>
                    </div>
                    <div class="col l10 m12 s12" id="testimonial">
                        <div class="carousel" style="overflow:hidden;">
                          
                          
                          
                          <c:if test="${not empty testimonialsList}">
			<c:forEach var="testimonial" items="${testimonialsList}">
				<a class="carousel-item" href="#two!">
					<div class="card small horizontal card-border">
						<div class="card-stacked">

							<div class="card-content white-text"
								style="background-image: url('resources/image/6388.jpg'); border-radius: 20px;">
								<br>
										<h5 class="white-text" style="margin-top: 10%;">${testimonial.content}</h5>
										<h5 class="right-align">- ${testimonial.givenByName}</h5>
							</div>
						</div>
					</div>
				</a>
			</c:forEach>
			</c:if> 



                        </div>
                    </div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                  <!--   <div class="col l10 m12 s12" id="testimonial">
                        <div class="carousel" style="overflow:hidden;">
                            <a class="carousel-item" href="#two!">
                                <div class="card small horizontal card-border">
                                    <div class="card-stacked">
                                        <div class="card-content white-text" style="background-image: url('resources/image/6388.jpg');border-radius:20px;">
                                            <br>
                                            <h5 class="white-text" style="margin-top:15%;">We got the perfect product at the perfect price.</h5>
                                            <h5 class="right-align">- Vimal Textile.</h5>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="carousel-item" href="#two!">
                                <div class="card small horizontal card-border">
                                    <div class="card-stacked">
                                        <div class="card-content white-text" style="background-image: url('resources/image/6388.jpg');border-radius:20px;">
                                            <br>
                                            <h5 class="white-text" style="margin-top:15%;">The quality of all Blue Square products is really good.</h5>
                                            <h5 class="right-align">- Amit Imitation Jewellery.</h5>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="carousel-item" href="#two!">
                                <div class="card small horizontal card-border">
                                    <div class="card-stacked">
                                        <div class="card-content white-text" style="background-image: url('resources/image/6388.jpg');border-radius:20px;">
                                            <br>
                                            <h5 class="white-text">We don't need to go for any other brand, Blue Square gives us better quality products in reasonable price.</h5>
                                            <h5 class="right-align">- Cheval Engineering.</h5>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="carousel-item" href="#two!">
                                <div class="card small horizontal card-border">
                                    <div class="card-stacked">
                                        <div class="card-content white-text" style="background-image: url('resources/image/6388.jpg');border-radius:20px;">
                                            <br>
                                            <h5 class="white-text">Blue Square is really focusing on customer satisfaction. My all customers are really satisfied with products.</h5>
                                            <h5 class="right-align">- Tirupati Industrial Ltd.</h5>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="carousel-item" href="#two!">
                                <div class="card small horizontal card-border">
                                    <div class="card-stacked">
                                        <div class="card-content white-text" style="background-image: url('resources/image/6388.jpg');border-radius:20px;">
                                            <br>
                                            <h5 class="white-text" style="margin-top:15%;">Blue Square is on its way to becoming a brand by all its features.</h5>
                                            <h5 class="right-align">- Vedika Ambedkar Architect.</h5>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <a class="carousel-item" href="#two!">
                                <div class="card small horizontal card-border">
                                    <div class="card-stacked">
                                        <div class="card-content white-text" style="background-image: url('resources/image/6388.jpg');border-radius:20px;">
                                            <br>
                                            <h5 class="white-text" style="margin-top:10%;">The best after-sale service you will ever get, which most companies fail to give.</h5>
                                            <h5 class="right-align">- Rander Corporation Ltd.</h5>
                                        </div>
                                    </div>
                                </div>
                            </a>

                        </div>
                    </div> -->
                    <div class="col l1 m6 s6">
                        <a id="nextButton" class="waves-effect waves-light" style="transform: translate(0,140px)">
                            <i class="large material-icons black-text grey lighten-2">navigate_next</i></a>
                    </div>
                </div>
                 
            </div>
            <!-- carousel for mobile device starts hide-on-large-only-->
            <div class="row hide-on-large-only">
            <div class="col s12 m12 l12">
       
                <h3 class="center">Testimonial</h3>
            </div>
                
                <div class="grey lighten-4 z-depth-1 aniview" data-av-animation="slideInUp">
                    <div class="col l1 m6 s6"> <a id="prevBtn" class="waves-effect waves-light"><i class="large material-icons black-text">navigate_before</i></a></div>
                    <div class="col l1 m6 s6"> <a id="nextBtn" class="waves-effect waves-light right"><i class="large material-icons black-text">navigate_next</i></a></div>
                    <div class="col l10 m12 s12" id="testimonial">
                        <div class="carousel" style="overflow:hidden;">
                           
                            <c:if test="${not empty testimonialsList}">
			<c:forEach var="testimonial" items="${testimonialsList}">
				<a class="carousel-item" href="#two!">
					<div class="card small horizontal card-border">
						<div class="card-stacked">

							<div class="card-content white-text"
								style="background-image: url('resources/image/6388.jpg'); border-radius: 20px;">
								<br>
										<h5 class="white-text" style="margin-top: 10%;">${testimonial.content}</h5>
										<h5 class="right-align">- ${testimonial.givenByName}</h5>
							</div>
						</div>
					</div>
				</a>
			</c:forEach>
			</c:if>
			

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--testimonial end-->
        
        <!--partners strat-->
        <div id="partnersDiv" class="sectionDiv">
            <br><br><br><br><br><br><br>
            <h3 class="center">Our Partners</h3>
            <!--PArtners carousel for large device starts -->
            <div class="row hide-on-med-and-down  aniview" data-av-animation="slideInUp" style="margin-top:1%;margin-bottom:1%;">

                <div class="col l1 m1 s1">
                    <a id="prevButton1" class="waves-effect waves-light" style="transform: translate(0,55px)">
                        <i class="medium material-icons black-text grey lighten-2">navigate_before</i></a>
                </div>
                <div class="col l10 m10 s10" id="partners">
                    <div class="carousel" style="overflow:hidden;">
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/allahabad.jpg');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/abhyudaya.jpg');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/axisBank.jpg');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/BOI.jpg');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal  white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/CentralBank.png');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal  white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/Citibank.png');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/CorporationBank.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/denaBank.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/hdfc.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/hsbc.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/icici.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/idbi.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/kotak.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/maharashtra.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/oriental.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/punjabNational.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/rbl.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/saraswat.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/sbi.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/svc.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/syndicate.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/theCooprative.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/union.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/YesBank.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col l1 m1 s1">
                    <a id="nextButton1" class="waves-effect waves-light" style="transform: translate(0,55px)">
                        <i class="medium material-icons black-text grey lighten-2">navigate_next</i></a>
                </div>
            </div>
            <!--Partners carousel for mobile device starts -->
            <div class="row hide-on-large-only  aniview" data-av-animation="slideInUp" style="margin-top:5%;margin-bottom:5%;">

                <div class="col l1 m6 s6">
                    <a id="prevBtn1" class="waves-effect waves-light" style="transform: translate(0,55px)">
                        <i class="medium material-icons black-text ">navigate_before</i></a>
                </div>
                <div class="col l1 m6 s6  ">
                    <a id="nextBtn1" class="waves-effect waves-light right" style="transform: translate(0,55px)">
                        <i class="medium material-icons black-text ">navigate_next</i></a>
                </div>

                <div class="col l10 m12 s12 " id="partners">
                    <div class="carousel" style="overflow:hidden;">
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/allahabad.jpg');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/abhyudaya.jpg');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/axisBank.jpg');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/BOI.jpg');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal  white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/CentralBank.png');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal  white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/Citibank.png');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/CorporationBank.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/denaBank.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/hdfc.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/hsbc.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/icici.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/idbi.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/kotak.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/maharashtra.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/oriental.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/punjabNational.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/rbl.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/saraswat.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/sbi.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/svc.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/syndicate.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/theCooprative.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/union.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/YesBank.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>

            </div>
            	<!--footer start-->

					<%@include file="components/footer.jsp"%>
				<!--footer end-->
        </div>
        <!--partners end-->

	<!--action button strat-->
	<div class="fixed-action-btn">
		<a class="btn-floating btn-large black"> <i
			class="large material-icons">view_list</i>
		</a>
		<ul>
			<li><a class="btn-floating red tooltipped" data-position="left"
				data-delay="50" data-tooltip="Emi Calculator" href="emiPageForWebsite"><i
					class="material-icons">insert_chart</i></a></li>
			<li><a class="btn-floating yellow darken-1 tooltipped"
				data-position="left" data-delay="50" data-tooltip="Contact Us"
				href="contactPageForWebsite"><i class="material-icons">perm_contact_calendar</i></a></li>
			<li><a id="modal" class="btn-floating green modal-trigger tooltipped"
				data-position="left" data-delay="50" data-tooltip="Call Me"
				href="#contactForm"><i class="material-icons">phone</i></a></li>

		</ul>
	</div>


	<!-- Modal Structure -->
	
	<div id="contactForm" class="modal modal-fixed-footer grey lighten-3">
	
		<form method="post" action="addingUserDetailsForWebsiteClientsModal" id="addUserForm" name="contactForm">
		<div class="row modal-content">
			<h4 class="center">Let Us Contact You</h4>
			<hr>
			
			<div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">people</i>
					 <input id="userDetailsName" name="userDetailsName" type="text" class="validate text" required value="" title="Enter Valid Name">
					 <label for="userDetailsName">Name</label>
				</div>
				
			
				<div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">email</i>
					 <input id="emailId" name="emailId" type="email" class="validate" required value="" title="Enter Valid Email"> 
					 <label for="emailId">Email</label>
				</div>
				
				
				<div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">phone</i>
					 <input id="mobileNumber" name="mobileNumber" type="text" class="validate num" required value="" title="Enter Valid Mobile Number">
					  <label for="mobileNumber">Phone</label>
				</div>
				
				 
				<div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">redeem</i> 
					<select multiple name="serviceId" id="selectService" required aria-required="true">
						<option value="" disabled>Choose your option</option>
						
						<c:if test="${not empty serviceList}">
						<c:forEach var="service" items="${serviceList}">
						
							<option value="<c:out value="${service.serviceId}" />"><c:out
									value="${service.serviceName}" /></option>
						</c:forEach>
						</c:if>
						
					</select> <label>Select Loan Type</label>
						
				</div>
				
		</div>

		<div class="row modal-footer grey lighten-3">
			<div class="col s6 m6 l6 offset-m1 offset-l1 offset-s3">
				<button type="submit" name="action" id="addUser"
					class="modal-action  btn waves-effect black">Submit</button>
			</div>
		</div>
		</form>
	</div>

	<!--action button end-->
	<div onclick="topFunction()" id="myBtn">
		<img src="resources/image/arrow-up-black.png" alt="scroll up" />
	</div>
</body>
</html>