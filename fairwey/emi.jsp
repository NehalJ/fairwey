
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<%@include file="components/header_import.jsp"%>

<!--for slider range-->
<script src="resources/js/nouislider.js"></script>
<!--<script src="resources/js/nouislider.min.js"></script>-->

<!--high chart for -->
<script src="https://code.highcharts.com/highcharts.js"></script>

<!--for slider range-->
<link rel="stylesheet" href="resources/css/nouislider.css">

<style>
.noUi-connect {
	background: #ff7474;
	border-radius: 10px;
	box-shadow: inset 0 0 3px rgba(51, 51, 51, 0.45);
	-webkit-transition: background 450ms;
	transition: background 450ms;
}

.noUi-horizontal .noUi-handle {
	width: 34px;
	height: 16px;
	left: -17px;
	top: -3px;
}

.noUi-target {
	background: #FAFAFA;
	border-radius: 10px;
	border: 1px solid #D3D3D3;
	box-shadow: inset 0 1px 1px #F0F0F0, 0 3px 6px -5px #BBB;
}

.noUi-horizontal {
	height: 12px;
}

.noUi-handle:before, .noUi-handle:after {
	content: "";
	display: block;
	position: absolute;
	height: 5px;
	width: 1px;
	background: #E8E7E6;
	left: 14px;
	top: 6px;
}

table, td, th {
	border: 1px solid #9e9e9e !important;
	font-size: 13px;
	/*border-width: 1px;*/
}

tbody tr:hover {
	background-color: #bdbdbd !important;
	z-index: 2;
}

.peach {
	background-color: #ff7474;
}

label {
	color: black;
}
 @media only screen and (max-width:600px){
.noUi-value {
    position: absolute;
    white-space: nowrap;
    text-align: center;
    font-size: 10px;
}

}
</style>
<script>
$(document).ready(function(){
	$(".dropdown-content li").click(function(){	 
        var a= $(".dropdown-content li").hasClass('active');
 		if(a === true){
 			
 			$("#selectService-error").css("display","none");
 		}
 		else if(a === false)
 				 {
 			$("#selectService-error").css("display","block");
 				 }
 		 
 	 });
});
</script>
</head>
<body>
	<!--navbar start-->
	<%@include file="components/navbar.jsp"%>
	<!--navbar end-->


	<!--content start-->
	<div class="row">
		<div class="col s12 m12 l12">
			<h4 class="center">Emi Calculator</h4>
			<br>
		</div>

		<div class="col s12 m7 l5 offset-l1 ">
			<div class=" col s12 m10 l10 offset-l1">
				<div id="lamount"></div>
				<br> <br>
				<div class="col s12 m6 l6">
					<label for="lamountpara"> Loan Amount is(Rs.)</label> <input type="text"
						id="lamountpara" required> <br> <br> <br>
				</div>

			</div>	
			<div class=" col s12 m10 l10 offset-l1">
			<div class="col s12 m6 l6 offset-l3">
				<input name="month" type="radio" id="month" value="month" checked />
					<label for="month">month</label> 
					<input name="month" type="radio" id="year" value="year" /> 
					<label for="year">year</label> 
			</div><br><br><br>
				<div id="noOfMonth"></div>
				<br> <br>
				<div class="col s12 m6 l6">
					<label for="noOfMonthpara"><span id="labelForyear">Number of month is</span></label>
					 <input type="text" id="noOfMonthpara" required> <br> <br>
					<br>
				</div>
			</div>
			<div class=" col s12 m10 l10 offset-l1">
				<div id="roi"></div>
				<br> <br>
				<div class="col s12 m6 l6">
					<label for="roipara"> Rate of interest is(%)</label> <input
						type="text" id="roipara" required> <br> <br> <br>
				</div>
			</div>
		</div>
		<div class="col s12 m5 l5 offset-l1 ">
			<div id="container" class="center-align  greycolor"
				style="height: 350px"></div>
			<!--<div id="container"></div>-->
		</div>
		<div class="col s12 m12 l10 offset-l1 ">
			<div class="col s12 m3 l3">
				<div class="card grey lighten-3">
					<div class="card-content teal-text center">
						<p class="card-title">Monthly Emi</p>

						<p class="card-title">
							&#8377;<span id="emipara"></span>
						</p>
					</div>
				</div>

			</div>
			<div class="col s12 m3 l3">
				<div class="card grey lighten-3">
					<div class="card-content teal-text center">
						<p class="card-title">Total Interest</p>
						<p class="card-title">
							&#8377;<span id="totalInterest"></span>
						</p>
					</div>
				</div>
			</div>
			<div class="col s12 m3 l3">
				<div class="card grey lighten-3">
					<div class="card-content teal-text center">
						<p class="card-title">Payble Amount</p>
						<p class="card-title">
							&#8377;<span id="paybleAmtPara"></span>
						</p>
					</div>
				</div>
			</div>
			<div class="col s12 m3 l3">
				<div class="card grey lighten-3">
					<div class="card-content teal-text center">
						<p class="card-title">Interest Percentage</p>
						<p class="card-title">
							<span id="interestPercentage"></span>%
						</p>
					</div>
				</div>
			</div>
		</div>

		<div class="col s12 m12 l10 offset-l1">
			<table class="striped highlight centered" id="tblData"
				cellspacing="0" width="100%">
				<thead>
					<tr class="grey">
						<th width="10%;">Payment No.</th>
						<th width="10%;">Principal</th>
						<th width="10%;">Interest</th>
						<th width="10%;">EMI</th>
						<th width="10%;">Balance</th>
					</tr>
				</thead>
				<tbody id="tblcontent">
				</tbody>
			</table>
		</div>
		
	</div>
	<div>
	<div class="col s12 m12 l12">	
		
	<%@include file="components/footer.jsp"%>
	</div>
	</div>
	<!--action button strat-->
	<div class="fixed-action-btn">
		<a class="btn-floating btn-large black"> <i
			class="large material-icons">view_list</i>
		</a>
		<ul>
			<li><a class="btn-floating red tooltipped" data-position="left"
				data-delay="50" data-tooltip="Emi Calculator" href="emiPageForWebsite"><i
					class="material-icons">insert_chart</i></a></li>
			<li><a class="btn-floating yellow darken-1 tooltipped"
				data-position="left" data-delay="50" data-tooltip="Contact Us"
				href="contactPageForWebsite"><i class="material-icons">perm_contact_calendar</i></a></li>
			<li><a class="btn-floating green modal-trigger tooltipped"
				data-position="left" data-delay="50" data-tooltip="Call Me"
				href="#contactForm"><i class="material-icons">phone</i></a></li>

		</ul>
	</div>


	<!-- Modal Structure -->
	<div id="contactForm" class="modal modal-fixed-footer grey lighten-3">
		<form method="post" action="addingUserDetailsForWebsiteEMIPageModal"
			id="addUserForm" name="contactForm">
			<div class="row modal-content">
				<h4 class="center">Let Us Contact You</h4>
				<hr>


				<div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">people</i> <input
						id="userDetailsName" name="userDetailsName" type="text"
						class="validate text" required value="" title="Enter Valid Name">
					<label for="userDetailsName">Name</label>
				</div>

				<div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">email</i> <input id="emailId"
						name="emailId" type="email" class="validate" required> <label
						for="emailId">Email</label>
				</div>

				<div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">phone</i> <input id="mobileNumber"
						name="mobileNumber" type="text" class="validate num" required>
					<label for="mobileNumber">Phone</label>
				</div>


				<div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">redeem</i> <select multiple
						name="serviceId" id="selectService" required aria-required="true">
						<option value="" disabled>Choose your option</option>

						<c:if test="${not empty serviceList}">
							<c:forEach var="service" items="${serviceList}">

								<option value="<c:out value="${service.serviceId}" />"><c:out value="${service.serviceName}" /></option>
							</c:forEach>
						</c:if>

					</select> <label>Select Loan Type</label>

				</div>




			</div>

			<div class="row modal-footer grey lighten-3">
				<div class="col s6 m6 l6 offset-m1 offset-l1 offset-s3">
					<button type="submit" name="action" id="addUser" href="#!"
						class="modal-action  btn waves-effect black">Submit</button>
				</div>
			</div>
		</form>
	</div>


	<!--action button end-->

	<div onclick="topFunction()" id="myBtn">
		<img src="resources/image/arrow-up-black.png" alt="scroll up" />
	</div>
	<!--content end-->
	<!--script for emi calulator start-->
	<script src="resources/js/wNumb.js"></script>
	<script src="resources/js/nouislider.js"></script>
	<script>
        var laSlider = document.getElementById("lamount");
        var lainput = document.getElementById("lamountpara");
        var monthSlider = document.getElementById("noOfMonth");
        var monthinput = document.getElementById("noOfMonthpara");
        var roiSlider = document.getElementById("roi");
        var roiinput = document.getElementById("roipara");
        var year = document.getElementById('year');
    	var month = document.getElementById('month');

    	 function updateSliderRange ( min, max ) {
    		 monthSlider.noUiSlider.updateOptions({
    	    		range: {
    	    			'min': min,
    	    			'max': max
    	    		}
    	    	});
    	    }
    	    	
    	    year.addEventListener('click', function(){
    	    	updateSliderRange(0, 30);
    	    });

    	    month.addEventListener('click', function(){
    	    	updateSliderRange(0, 360);
    	    });
    	
        // loan Amount  slider
        noUiSlider.create(laSlider, {
                start: [500000],
                connect: 'lower', // direction: 'rtl',
                range: {
                    'min': 0,
                    'max': 20000000
                },
                tooltips: true,
                format: wNumb({
                    decimals: 0,
                    thousand: ',',
                    prefix: ''
                }),
                pips: {
                    mode: 'count',
                    density: 1,
                    values: 6,
                }
            }

        );
      

   
        // month Slider
        noUiSlider.create(monthSlider, {
                start: [12],
                connect: 'lower', // direction: 'rtl',
                range: {
                    'min': 0,
                    'max': 360
                },
                tooltips: true,
                format: wNumb({
                    decimals: 0,
                    thousand: ',',
                    prefix: ''
                }),
                pips: {
                    mode: 'count',
                    density: 1,
                    values: 6,

                }
            }

        );

        // roi slider

        noUiSlider.create(roiSlider, {
                start: [7],
                connect: 'lower', // direction: 'rtl',
                range: {
                    'min': 5,
                    'max': 30,
                },
                tooltips: true,
                format: wNumb({
                    decimals: 2,
                    thousand: ',',
                    prefix: ''
                }),
                pips: {
                    mode: 'count',
                    density: 1,
                    values: 6,

                }
            }

        );

     // loan Amount  slider
        lainput.addEventListener('blur', function() {
                //  alert(this.value);
                laSlider.noUiSlider.set(this.value);

            }

        );
        laSlider.noUiSlider.on('update', function(values) {
                // alert(values);
                lainput.value = values;
                emiCal();
            }

        );
       
        // month Slider

         
        
        monthinput.addEventListener('blur', function() {
            //  alert(this.value);
            monthSlider.noUiSlider.set(this.value);

        }

    );
        monthSlider.noUiSlider.on('update', function(values) {
                // alert(values);
                monthinput.value = values;
                emiCal();
            }

        );

        
     // roi slider
        roiinput.addEventListener('blur', function() {
                //  alert(this.value);
                roiSlider.noUiSlider.set(this.value);
            }

        );
        roiSlider.noUiSlider.on('update', function(values) {
                // alert(values);
                roiinput.value = values;
                emiCal();
            }

        );

        $(document).ready(function() {
            emiCal();
        });

        function emiCal() {
            // for remove comma
            var num = $("#lamountpara").val();
            num = num.replace(/,/g, "");
            // alert(num);
            var loanAmount = num;
            var abc;
            var numberOfMonths;
            var monthOrYear = $("#noOfMonthpara").val();
            if($("input[name='month']:checked").val()==="month"){
        		//alert("month");
        		abc=monthOrYear
        	}
        		else if($("input[name='month']:checked").val()==="year"){
        			//alert("year");
        			abc=monthOrYear*12;
        		}    
            $("input[name='month']").change(function(){
            //	console.log($("input[name='month']").val());
            	if($("input[name='month']:checked").val()==="month"){
            		$("#labelForyear").text("Number of month is");
            	}
            		else if($("input[name='month']:checked").val()==="year"){
            			//alert("year");
            			abc=monthOrYear*12;
            			//$("#noOfMonthpara").val(abc);		
            			numberOfMonths = abc;
            			$("#labelForyear").text("Number of Year is");
            		}           	
            });
            numberOfMonths = abc;
           // var numberOfMonths = $("#noOfMonthpara").val();
            var rateOfInterest = $("#roipara").val();
            var monthlyInterestRatio = (rateOfInterest / 100) / 12;
            //alert(monthlyInterestRatio);

            // top part of formula (1+roi)powerOf(n)
            var top = Math.pow((1 + monthlyInterestRatio), numberOfMonths);
            //alert(top);

            // bottom part of formula ((1+roi)powerOf(n)-1)
            var bottom = top - 1;
            // alert(bottom);
            var sp = top / bottom;
            //alert(sp);
            var emi = ((loanAmount * monthlyInterestRatio) * sp);
         //   console.log(emi);
            var paybleAmt = emi * numberOfMonths;
            var Totalinterest = paybleAmt - loanAmount;
            var interestPercentage = (Totalinterest / paybleAmt) * 100;

            // set the values total interest and payble amount 
            $("#emipara").text(Math.round(emi));
            $("#totalInterest").text(Math.round(Totalinterest));
            $("#paybleAmtPara").text(Math.round(paybleAmt));
            $("#interestPercentage").text(Math.round(interestPercentage));


            // calculating table content
            var beginingAmt = parseInt(loanAmount);
            var interestAmt = 0;
            var principle = 0;
            var endingAmt = 0;
            var a = 0;
            // Return today's date and time
            var currentTime = new Date()
                // returns the year (four digits)
            var year = currentTime.getFullYear();
            //returns current month
            var currentMonth = currentTime.getMonth();
           
          	//var currentMonth=10;
            //we calculate c to iterrate 1st year
            var c = 12 - currentMonth;
            // calcuate j to iterate last year
            //var jNumber = 13 - c;
            //calcuates how many times outer loop should rotate
            //var iNumber = numberOfMonths - c;
             var exitLastLoop;
            var yearData = 0;
            var newMonth;
            var monthNames = ["January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December"
            ];
            //outer loop for printing year n times given by user
            var tblcontent = "";
            for (var i = 1; i <= numberOfMonths; i++) {
                if (c > 0 && c<numberOfMonths) {
                    for (var x = 0; x < c; x++) {
                        interestAmt = beginingAmt * ((rateOfInterest / 100) / 12);
                        principle = emi - interestAmt;
                        endingAmt = beginingAmt - principle;
                        yearData = year;
                        var setMonth = currentMonth + x;
                        tblcontent += "<tr><td>" + yearData + "    " + monthNames[setMonth] + "</td><td>" + Math.round(principle) + "</td><td> " + Math.round(interestAmt) + "</td><td>" + Math.round(emi) + "</td><td>" + Math.round(endingAmt) + "</td></tr>";
                        console.log(yearData);
                        beginingAmt = beginingAmt - principle;
                        //  arr.push(year);
                        a = 13;
                        i = i + 1;
                    }
                    //c set to 0 so as to disable loop of 1st year
                    c = 0;
                    // this variable executes last loop if number months are greater than 12
                    exitLastLoop=1;
                }
                else if(c > 0 && c>=numberOfMonths){
                	   for (var x = 0; x < numberOfMonths; x++) {
                           interestAmt = beginingAmt * ((rateOfInterest / 100) / 12);
                           principle = emi - interestAmt;
                           endingAmt = beginingAmt - principle;
                           yearData = year;
                           var setMonth = currentMonth + x;
                           tblcontent += "<tr><td>" + yearData + "    " + monthNames[setMonth] + "</td><td>" + Math.round(principle) + "</td><td> " + Math.round(interestAmt) + "</td><td>" + Math.round(emi) + "</td><td>" + Math.round(endingAmt) + "</td></tr>";
                           console.log(yearData);
                           beginingAmt = beginingAmt - principle;
                           //  arr.push(year);
                           a = 13;
                           i = i + 1;
                       }
                       //c set to 0 so as to disable loop of 1st year
                       c = 0;   
                       // this variable prevents last loop to execute if number months are less than 12
                       exitLastLoop=0;
                      
                }
               if( exitLastLoop===1){
                if (a >= 12) {
                    year = year + 1;
                    a = 0;
                }
                //prints year
                yearData = year;
                //reset month number after reaching december
                if (setMonth >= 11) {
                    setMonth = 0;
                }
                if (newMonth >= 11) {
                    newMonth = 0;
                }
                newMonth = setMonth + a;
                interestAmt = beginingAmt * ((rateOfInterest / 100) / 12);
                principle = emi - interestAmt;
                endingAmt = beginingAmt - principle;
                tblcontent += "<tr><td>" + yearData + "    " + monthNames[newMonth] + "</td><td>" + Math.round(principle) + "</td><td> " + Math.round(interestAmt) + "</td><td>" + Math.round(emi) + "</td><td>" + Math.round(endingAmt) + "</td></tr>";
                console.log(yearData);
                a = a + 1;
                beginingAmt = beginingAmt - principle;
                //arr.push(year);
               // return false;
            }
               else{
               	
               }
            }
            
            $("#tblcontent").html(tblcontent);
            $("#tblcontent").change();
            $('#container').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {
                    text: 'EMI Calculator'
                },
                colors: ['#ff7474', '#b0bec5'

                ],
                tooltip: {
                    //pointFormat: '{series.name}: <b>{point.value}%</b>'
                },
                //for remove chart linked to highchart.com
                credits: ({
                    enabled: false
                }),
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            //	enabled: true,
                            color: '#000000',
                            connectorColor: '#000000',
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Amount',
                    data: [
                        ['Loan', eval(loanAmount)],
                        ['Interest', eval(Math.round(Totalinterest))]
                    ]
                }]
            });
        }
    </script>

	<!--script for emi calulator End-->

</body>
</html>