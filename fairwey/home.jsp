<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
  <!DOCTYPE html>
<html>

<head>
     <%@include file="components/header_import.jsp"%>
    <style>
        #servicesDiv,
        #testimonialDiv,
        #partnersDiv {
            height: 600px;
        }
        
        #emiDiv {
            height: 260px;
            margin-bottom: 0;
            margin-top: 5%
        }
        
        #emiDiv img {
            height: 250px;
        }
        /*for services box*/
        
        #servicesDiv.row .col {
            box-sizing: border-box;
            padding: 6px .75rem;
            min-height: 1px;
        }
        
        .parallax-container {
            position: relative;
            overflow: hidden;
            height: 662px !important;
        }
        
        #servicesDiv {
            margin-left: auto;
        }
      /*   .sectionDiv{
        height:100%;
        } */
        .card-title {
            height: 50px;
            font-size: 16px !important;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
            width: 100%;
            padding: 3px 0 3px 6px;
            font-weight: 400 !important;
        }
        
        @media only screen and (max-width:600px) {
            #emiDiv {
                margin-bottom: 0;
                margin-top: -12%;
                height: 300px;
            }
            #emiDiv img {
                height: 210px;
            }
        }
        
        .parallax-container {
            /*position: relative;*/
            /*overflow: hidden;*/
            /*height: 600px;*/
            /*for background black effect*/
            background-color: rgba(0, 0, 0, 0.6);
        }
        
        .parallax img {
            /*-webkit-filter: sepia(1);
            filter: sepia(1);*/
            -webkit-filter: grayscale(1);
            filter: grayscale(1%);
        }
        
        .card {
            border-radius: 5px;
        }
        
        .card-title {
            transition: box-shadow .25s;
        }
        
        .card .card-content {
            padding: 10px;
        }
        
        .slider .slides {
            background-color: white;
        }
       
        @media only screen and (max-width:600px) {
            #testimonial .carousel {
                overflow: visible;
                position: relative;
                width: 100%;
                height: 360px;
                -webkit-perspective: 500px;
                perspective: 500px;
                -webkit-transform-style: preserve-3d;
                transform-style: preserve-3d;
                -webkit-transform-origin: 0% 50%;
                transform-origin: 0% 50%;
            }
            #testimonial .carousel .carousel-item {
                display: none;
                width: 310px;
                height: 280px;
                position: absolute;
                top: 0;
                left: 0;
                margin-top: -8%;
                opacity: 1 !important;
            }
            #testimonial .card {
                max-height: 80%;
            }
        }
        
        @media only screen and (max-width:992px) {
            #emiDiv {
                margin-top: 30%;
                height: 450px;
            }
            #emiDiv img {
                margin-bottom: 0;
                margin-top: -15%;
                height: 310px;
            }
            #servicesDiv {
                margin-left: 5%;
            }
            .card-title {
                height: 72px;
                font-size: 16px !important;
                border-top-left-radius: 5px;
                border-top-right-radius: 5px;
                width: 100%;
                padding: 3px 0 3px 6px;
                font-weight: 400 !important;
            }
        }
        /*#currentRate .card {
            height: 100x;
        }
        
        #currentRate .card-content {
            font-size: 16px;
        }*/
    </style>
    <script>
      
        $(document).ready(function() {
        	
        	
                 if ($(window).width() >= 770) {
                     $.scrollify({
                         section: ".sectionDiv",
                         sectionName: ".sectionDiv",
                         easing: "easeOutExpo",
                         setHeights: true,
                         overflowScroll: true,
                         scrollSpeed: 1100,
                         offset: 0,
                         scrollbars: true,
                         before: function() {
                        	 $.scrollify.current().css({'height':$(window).height()});
                         },
                         after: function() {},
                         afterResize: function() {}
                     });
                 }
           
        	 $(".dropdown-content li").click(function(){	 
                 var a= $(".dropdown-content li").hasClass('active');
          		if(a === true){
          			
          			$("#selectService-error").css("display","none");
          		}
          		else if(a === false)
          				 {
          			$("#selectService-error").css("display","block");
          				 }
          		 
          	 });
        		 
        	
        	//window.location.href="${pageContext.request.contextPath}/homePageForWebsite";
        	//history.pushState('data', '', '${pageContext.request.contextPath}/homePageForWebsite');
            // $('.slider').slider();
            
            $(".leftclick").click(function() {

                $('.slider').slider('prev');
            });
            $(".rightclick").click(function() {

                $('.slider').slider('next');
            });

            $(".button-collapse").sideNav();
            $('#partners .carousel').carousel({

                duration: 300,
                padding: 30,
                //  use this to increase distance between images
                dist: 0
            });
            autoplayPartners();

            function autoplayPartners() {
                $('#partners .carousel').carousel('next');
                setTimeout(autoplay, 5000);

            };

            $('#prevButton1').click(function() {
                $('#partners .carousel').carousel('prev');
            });
            $('#nextButton1').click(function() {
                $('#partners .carousel').carousel('next');
            });
            $('#prevBtn1').click(function() {
                $('#partners .carousel').carousel('prev');
            });
            $('#nextBtn1').click(function() {
                $('#partners .carousel').carousel('next');
            });
            $('.carousel').carousel({
                duration: 100,
                padding: 30,
                //  use this to increase distance between images
                dist: 0
            });
            autoplay();


            function autoplay() {
                $('.carousel').carousel('next');
                setTimeout(autoplay, 5000);

            };
            $('#prevButton').click(function() {
                $('#testimonial .carousel').carousel('prev');
            });
            $('#nextButton').click(function() {
                $('#testimonial .carousel').carousel('next');
            });
            $('#prevBtn').click(function() {
                $('#testimonial .carousel').carousel('prev');
            });
            $('#nextBtn').click(function() {
                $('#testimonial .carousel').carousel('next');
            });

            $('.parallax').parallax();

        });
    </script>

</head>

<body>
    <!--navbar start-->
		<%@include file="components/navbar.jsp"%>
	<!--navbar end-->
	
    <!--content start-->

      <!--slider full screen start-->
    <div class="slider fullscreen sectionDiv">
        <br>
        <!--start slides-->
        <ul class="slides">
            <li>
                <img src="resources/image/structure.jpg">
                <!-- random image -->
                <div class="caption right-align">
                    <h3 class="black-text">Right Aligned Caption</h3>
                    <h5 class="light black-text">Here's our small slogan.</h5>
                </div>
                <!--called to action buuton over slider-->
                <div class="caption" style="margin-top:30%;margin-left:30%;">
                    <a class="btn btn-waves red" href="services.html">Abc</a>
                </div>
            </li>
            <!--<li>
                <img src="resources/image/finalpics/cgtmse1.jpg">-->
            <!-- random image -->
            <!--<div class="caption left-align">
                    <h3 class="black-text">CGTMSE</h3>
                    <h5 class="light black-text">We can arrange unsecured CC,OD & Term Loan up to 200 lacs for new and existing manufacturing companies under the govt. scheme</h5>
                </div>
            </li>-->

            <li>
                <img src="resources/image/nonFuntoFund.jpg" >
                <!-- random image -->
                <div class="caption left-align">
                    <h3 class="black-text">Left Aligned Caption</h3>
                    <h5 class="light black-text">Here's our small slogan.</h5>
                </div>
            </li>

            <li style="">
                <img src="resources/image/slider3.jpg" style="-webkit-filter: hue-rotate(156deg);filter: hue-rotate(156deg);">
                <!-- random image -->
                <div class="caption center-align">
                    <h3 class="black-text">This is our big Tagline!</h3>
                    <h5 class="light black-text">Here's our small slogan.</h5>
                </div>
            </li>

            <li>
                <img src="resources/image/loanAgainstProperty.jpg">

                <!-- random image -->
                <div class="caption center-align">
                    <h3 class="black-text">This is our big Tagline!</h3>
                    <h5 class="light black-text">Here's our small slogan.</h5>
                </div>
                <!--called to action buuton over slider-->
                <div class="caption" style="margin-top:30%;margin-left:30%;">
                    <a class="btn btn-waves red" href="services.html">Abc</a>
                </div>
            </li>
        </ul>
        <!--end slides-->

        <!--ul for prev next arrow over slider start-->
        <!-- <div id="btnSideSlider" class="row" style="width:100%;">
            <div class="col l1 m1">
                <a class="leftclick" id="left"><i class="material-icons large grey-text text-darken-1 left">keyboard_arrow_left</i></a>
            </div>
            <div class="col l1 m1 offset-l10 offset-m10 hide-on-small-only">
                <a class="rightclick" id="right"><i class="material-icons large grey-text text-darken-1 right">keyboard_arrow_right</i></a>
            </div>
            <div class="hide-on-med-and-up">
                <a class="rightclick" id="right"><i class="material-icons large grey-text text-darken-2 right">keyboard_arrow_right</i></a>
            </div>
        </div> -->
    </div>
    <!--ul for prev next arrow over slider end-->
    <!--slider full screen end-->
   
    <main>
        <!--services start-->
        <div class="sectionDiv">
        <br><br><br> <br><br><br> 
        <div class="row  aniview " id="servicesDiv" data-av-animation="slideInUp">
            <div class="col s12 m12 l12">
               
                <h3 class="center">Services</h3>
            </div>
            <!--<div class="s12 m12 l10 offset-l1 offset-s8 offset-m6">-->
            <div class="col s3 m2 l2  sideborder center  offset-s1 ">
                <a href="servicePageForWebsite#structuredFinance"><img src="resources/image/structured.png" alt="">
                    <figcaption class="black-text">Structured Finance</figcaption>
                </a>
            </div>

            <div class="col s3 m2 l2 sideborder center ">
                <a href="servicePageForWebsite#letterofCredit"><img src="resources/image/cashcredit.png" alt="">
                    <figcaption class="black-text">Cash Credit </figcaption>
                </a>
            </div>
            <div class="col s3 m2 l2 sideborder center ">

                <a href="servicePageForWebsite#overDraft"><img src="resources/image/overdraft.png" alt="">
                    <figcaption class="black-text">Overdraft</figcaption>

                </a>

            </div>
            <div class="col s3 m2 l2  sideborder center offset-s1">
                <a href="servicePageForWebsite#termLoan"><img src="resources/image/termloan.png" alt="">
                    <figcaption class="black-text">Term Loan </figcaption>
                </a>

            </div>
            <div class="col s3 m2 l2  sideborder center ">
                <a href="servicePageForWebsite#projectFinance"><img src="resources/image/project.png" alt="">
                    <figcaption class="black-text">Project Finance</figcaption>
                </a>
            </div>
            <div class="col s3 m2 l2  sideborder center ">
                <a href="servicePageForWebsite#letterofCredit"><img src="resources/image/letterofcredit.png" alt="">
                    <figcaption class="black-text">Letter of Credit</figcaption>
                </a>
            </div>
            <div class="col s3 m2 l2  sideborder center offset-s1">
                <a href="servicePageForWebsite#exportbillDiscounting"><img src="resources/image/buyercredit.png" alt="">
                    <figcaption class="black-text">Buyers Credit</figcaption>
                </a>
            </div>
            <div class="col s3 m2 l2  sideborder center ">
                <a href="servicePageForWebsite#exportbillDiscounting"><img src="resources/image/billdiscount.png" alt="">
                    <figcaption class="black-text">Export Bill Discounting </figcaption>
                </a>
            </div>
            <div class="col s3 m2 l2  sideborder center ">
                <a href="servicePageForWebsite#exportpacking"><img src="resources/image/exportpacking.png" alt="">
                    <figcaption class="black-text">Export Packing Credit</figcaption>
                </a>
            </div>
            <div class="col s3 m2 l2  sideborder center offset-s1">
                <a href="servicePageForWebsite#purchaseBillDiscounting"><img src="resources/image/salesbill1.png" alt="">
                    <figcaption class="black-text">Purchase & Sales Bill Discounting</figcaption>
                </a>
            </div>
            <div class="col s3 m2 l2  sideborder center ">
                <a href="servicePageForWebsite#nonfund"><img src="resources/image/bank.png" alt="">
                    <figcaption class="black-text">Bank Guarantees</figcaption>
                </a>
            </div>
            <div class="col s3 m2 l2  sideborder center ">
                <a href="servicePageForWebsite#nonfund"><img src="resources/image/fund.png" alt="">
                    <figcaption class="black-text">Non Fund to Fund</figcaption>
                </a>
            </div>
            <div class="col s3 m2 l2  sideborder center offset-s1">
                <a href="servicePageForWebsite#homeLoan"><img src="resources/image/homeloan.png" alt="">
                    <figcaption class="black-text">Home Loan</figcaption>
                </a>
            </div>
            <div class="col s3 m2 l2  sideborder center ">
                <a href="servicePageForWebsite#loanAgainstProp"><img src="resources/image/loanagainstprop.png" alt="">
                    <figcaption class="black-text">Loan against Property</figcaption>
                </a>
            </div>
            <div class="col s3 m2 l2  sideborder center ">
                <a href="servicePageForWebsite#personalLoan"><img src="resources/image/business.png" alt="">
                    <figcaption class="black-text">Unsecured Business Loan</figcaption>
                </a>
            </div>
            <div class="col s3 m2 l2  sideborder center offset-s1">
                <a href="servicePageForWebsite#personalLoan"><img src="resources/image/personal.png" alt="">
                    <figcaption class="black-text">Personal Loan</figcaption>
                </a>
            </div>
            <div class="col s3 m2 l2  sideborder center ">
                <a href="servicePageForWebsite#leaseRentalDiscount"><img src="resources/image/discounting.png" alt="">
                    <figcaption class="black-text">Lease Rental Discounting</figcaption>
                </a>
            </div>
            <div class="col s3 m2 l2  sideborder center ">
                <a href="servicePageForWebsite#leaseRentalDiscount"><img src="resources/image/security.png" alt="">
                    <figcaption class="black-text">Loan Against Securities</figcaption>
                </a>
                <br><br>
            </div>

            <!--<div class="col s3 m2 l2  sideborder center">
                <a href=""><img src="resources/img/coin64.png" alt="">
                    <figcaption>OTHER SERVICES</figcaption>
                </a>
            </div>-->

            <!--</div>-->
        </div>
</div>
        <!--services end-->
        
        
        <!--current rates start-->
        <div class="row sectionDiv">
            <div class="col s12 m12 l12">
                <div class="container aniview " id="currentRate" data-av-animation="slideInUp">
                    <br> <br> <br> <br>
                    <h3 class="center">Current RBI Rates</h3>
                    <br> <br> <br>
                    <!--<div class="row">-->
                    <div class="col s12 l12 m12">
                        <ul class="tabs hide-on-small-only">
                            <li class="tab col s4 l4"><a href="#PolicyRates">Policy Rates</a></li>
                            <li class="tab col s4 l4" style="padding-left:5%;"><a class="active" href="#ReserveRatio">Reserve Ratio</a></li>
                            <li class="tab col s4 l4" style="padding-left:5%; padding-right:0;"><a href="#DepositeRates">Lending/Deposit Rates</a></li>
                        </ul>
                        <ul class="tabs hide-on-med-and-up" style="text-overflow:auto">
                            <li class="tab col s4 l4 "><a href="#PolicyRates">Policy Rates</a></li>
                            <li class="tab col s4 l4 "><a class="active" href="#ReserveRatio">Reserve Ratio</a></li>
                            <li class="tab col s4 l4 "><a href="#DepositeRates">Lending/Deposite Rates</a></li>
                        </ul>
                    </div>
                    <div id="PolicyRates" class="col s12 l11 m12">
                        <!--<h5 class="center">Policy Rates</h5>-->
                        <!--<hr style="width:50%;"><br>-->
                        <div class="col s12 l3 m3">
                            <div class="card grey lighten-3 ">
                                <div class="card-title center grey lighten-1">
                                    <span class="center black-text">Policy Repo Rate</span>

                                </div>
                                <div class="card-content center black-text ">

                                    <span>${policyRepoRate.ratesPercentage}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 l3 m3 ">
                            <div class="card grey lighten-3 ">
                                <div class="card-title center grey lighten-1">
                                    <span class="center black-text">Reverse Repo Rate</span>
                                </div>
                                <div class="card-content center black-text ">

                                    <span>${reverseRepoRate.ratesPercentage}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 l3 m3 ">

                            <div class="card  grey lighten-3" style="width:100%">
                                <div class="card-title center grey lighten-1">
                                    <span class="center black-text">Bank Rate</span>
                                </div>
                                <div class="card-content center black-text ">
                                    <span>${bankRate.ratesPercentage}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col s12 l3 m3 ">

                            <div class="card grey lighten-3" style="width:100%">
                                <div class="card-title center grey lighten-1">
                                    <span class="center black-text">Marginal Standing Facility Rate</span>
                                </div>
                                <div class="card-content center black-text ">
                                    <span>${marginalStandingFacilityRate.ratesPercentage}</span>
                                </div>
                            </div>

                        </div>
                    </div>



                    <div id="ReserveRatio" class="col s12 l11 m12">

                        <!--<h5 class="center">Reserve Ratio</h5>-->
                        <!--<hr style="width:50%;"><br>-->
                        <div class=" col s12 l3 m3 offset-l3 offset-m2">

                            <div class="card grey lighten-3">
                                <div class="card-title center grey lighten-1">
                                    <span class="center center black-text">cash reserve ratio</span>
                                </div>
                                <div class="card-content center black-text ">
                                    <span>${crr.ratesPercentage}</span>
                                </div>
                            </div>

                        </div>
                        <div class=" col s12 l3 m3">
                            <div class="card grey lighten-3">
                                <div class="card-title center grey lighten-1">
                                    <span class="center black-text">Statutory liquidity ratio</span>
                                </div>
                                <div class="card-content center black-text ">
                                    <span>${slr.ratesPercentage}</span>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div id="DepositeRates" class="col s12 l11 m12">

                        <!--<h5 class="center">Lending / Deposit Rates</h5>-->
                        <!--<hr style="width:50%;"><br>-->
                        <div class=" col s12 l3 m3">
                            <div class="card grey lighten-3">
                                <div class="card-title center grey lighten-1">
                                    <span class="center black-text">Base Rate </span>
                                </div>
                                <div class="card-content center black-text ">
                                    <span>${baseRate.ratesPercentage}</span>
                                </div>
                            </div>
                        </div>
                        <div class=" col s12 l3 m3">
                            <div class="card grey lighten-3">
                                <div class="card-title center grey lighten-1">
                                    <span class="center black-text">Marginal Cost of funds based Lending rate</span>
                                </div>
                                <div class="card-content center black-text ">
                                    <span>${mclr.ratesPercentage}</span>
                                </div>
                            </div>

                        </div>
                        <div class=" col s12 l3 m3">
                            <div class="card grey lighten-3">
                                <div class="card-title center grey lighten-1">
                                    <span class="center black-text">Savings Deposit Rate</span>

                                </div>
                                <div class="card-content center black-text ">

                                    <span>${savingsDepositRate.ratesPercentage}</span>
                                </div>
                            </div>

                        </div>
                        <div class=" col s12 l3 m3">
                            <div class="card grey lighten-3">
                                <div class="card-title center grey lighten-1">
                                    <span class="center black-text">Term Deposit Rate > 1 Year</span>
                                </div>
                                <div class="card-content center black-text ">
                                    <span>${termDepositRateYear.ratesPercentage}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!--</div>-->
            <div class=" col s12 m12 l12">
                <br> <br>
                <div style="border:4px solid black;position:relative;margin:0 5px 0 spx;">
                    <h4 class="center">
                        <i class="fa fa-quote-left" aria-hidden="true"></i> Price is what you pay .Value is what you get.<i class="fa fa-quote-right" aria-hidden="true"></i>
                    </h4>
                </div>
                <br>
                <!--</div>
                </div>-->
            </div>

        </div>
        <!--current rates End-->
        
        <!--why us start-->

        <div class="row sectionDiv">
            <div class="col s12 m12 l12" style="padding:0 0;">
                <div class="parallax-container  hide-on-med-and-down">
                    <div class="parallax"><img src="resources/image/whyus.jpg"></div>
                    <br><br> <br>
                    <div class="parallax-content white-text">
                        <br>
                        <h4 class="center">Why Fairwey Advisory?</h4>
                        <!--<img class=" center responsive-img headerimg" src="resources/img/header2-01.png " alt=" " style="margin-left:2%;">-->
                        <br><br>
                        <div class="row">
                            <div class="col s4 l4 m4 center">
                                <i class="material-icons  medium center-align">access_time</i>
                                <h5>Efficient And Timely Service</h5>
                            </div>
                            <div class="col s4 l4 m4 center">
                                <i class="material-icons medium">record_voice_over</i>
                                <h5> Reasonable Advice</h5>
                            </div>
                            <div class="col s4 l4 m4 center ">
                                <i class="material-icons medium">person_add</i>
                                <h5> Customer Centricity</h5>
                            </div>
                            <div class="col s4 l4 m4 center">
                                <i class="material-icons medium ">people</i>
                                <h5>Experienced and qualified team</h5>
                            </div>
                            <div class="col s4 l4 m4 center">
                                <i class="material-icons medium">work</i>
                                <h5>Reliable Service</h5>
                            </div>
                            <div class="col s4 l4 m4 center">
                                <i class="material-icons medium ">insert_drive_file</i>
                                <h5>Documentation support</h5>

                            </div>
                            <div class="col s4 l4 m4 center">
                                <i class="material-icons  medium center-align">event_available</i>
                                <h5>Most Comprehensive Planning</h5>
                            </div>
                            <div class="col s4 l4 m4 center">

                                <i class="material-icons medium">assignment</i>
                                <h5>Committed & Accountable</h5>
                            </div>
                            <div class="col s4 l4 m4 center">
                                <i class="material-icons medium">settings</i>
                                <h5>Charge on Success of Service</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="parallax-container hide-on-large-only" style="height:100%;">
                    <div class="parallax"><img src="resources/image/whyus.jpg"></div>
                    <br> <br> <br>
                    <div class="parallax-content white-text">
                        <h4 class="center">Why Fairwey Advisory?</h4>
                        <!--<img class=" center responsive-img headerimg" src="resources/img/header2-01.png " alt=" " style="margin-left:2%;">-->
                        <div class="row">
                            <div class="col s4 l4 m4 center">
                                <i class="material-icons  medium center-align">access_time</i>
                                <h6>Efficient And Timely Service</h6>
                            </div>
                            <div class="col s4 l4 m4 center">
                                <i class="material-icons medium">record_voice_over</i>
                                <h6> Reasonable Advice</h6>
                                <br>
                            </div>
                            <div class="col s4 l4 m4 center ">
                                <i class="material-icons medium">person_add</i>
                                <h6> Customer Centricity</h6>
                                <br>
                            </div>

                            <div class="col s4 l4 m4 center">
                                <i class="material-icons medium">work</i>
                                <h6>Reliable Service</h6>
                            </div>
                            <div class="col s4 l4 m4 center">
                                <i class="material-icons medium ">insert_drive_file</i>
                                <h6>Documentation support</h6>
                            </div>
                            <div class="col s4 l4 m4 center">
                                <i class="material-icons  medium center-align">event_available</i>
                                <h6>Most Comprehensive Planning</h6>
                            </div>
                            <div class="col s4 l4 m4 center">
                                <i class="material-icons medium">assignment</i>
                                <h6>Committed & Accountable</h6>
                            </div>
                            <div class="col s4 l4 m4 center">
                                <i class="material-icons medium">settings</i>
                                <h6>Charge on Success of Service</h6>
                            </div>
                            <div class="col s4 l4 m4 center">
                                <i class="material-icons medium ">people</i>
                                <h6>Experienced and qualified team</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--why us end-->

		<!--testimonial start-->
        <div id="testimonialDiv" class="aniview sectionDiv " data-av-animation="slideInUp">
            <div class="row hide-on-med-and-down">
                <br><br><br><br><br>
                <h3 class="center">Testimonial</h3>
                <!-- carousel for large device starts -->
                <div class=" grey lighten-4 z-depth-1 aniview" data-av-animation="slideInUp" style="margin-top:5%;margin-bottom:5%;">

                    <div class="col l1 m6 s6">
                        <a id="prevButton" class="waves-effect waves-light" style="transform: translate(0,140px)">
                            <i class="large material-icons black-text grey lighten-2">navigate_before</i></a>
                    </div>
                    <div class="col l10 m12 s12" id="testimonial">
                        <div class="carousel" style="overflow:hidden;">
                          
                          
                          
                          <c:if test="${not empty testimonialsList}">
			<c:forEach var="testimonial" items="${testimonialsList}">
				<a class="carousel-item" href="#two!">
					<div class="card small horizontal card-border">
						<div class="card-stacked">

							<div class="card-content white-text"
								style="background-image: url('resources/image/6388.jpg'); border-radius: 20px;">
								<br>
										<h5 class="white-text" style="margin-top: 10%;">${testimonial.content}</h5>
										<h5 class="right-align">- ${testimonial.givenByName}</h5>
							</div>
						</div>
					</div>
				</a>
			</c:forEach>
			</c:if> 



                        </div>
                    </div>
                    
     
                    <div class="col l1 m6 s6">
                        <a id="nextButton" class="waves-effect waves-light" style="transform: translate(0,140px)">
                            <i class="large material-icons black-text grey lighten-2">navigate_next</i></a>
                    </div>
                </div>
            </div>
            <!-- carousel for mobile device starts hide-on-large-only-->
            <div class="row hide-on-large-only">
                <br><br><br> <br><br><br> <br><br><br>
                <h3 class="center">Testimonial</h3>
                <div class="grey lighten-4 z-depth-1 aniview" data-av-animation="slideInUp">
                    <div class="col l1 m6 s6"> <a id="prevBtn" class="waves-effect waves-light"><i class="large material-icons black-text">navigate_before</i></a></div>
                    <div class="col l1 m6 s6"> <a id="nextBtn" class="waves-effect waves-light right"><i class="large material-icons black-text">navigate_next</i></a></div>
                    <div class="col l10 m12 s12" id="testimonial">
                        <div class="carousel" style="overflow:hidden;">
                           
                            <c:if test="${not empty testimonialsList}">
			<c:forEach var="testimonial" items="${testimonialsList}">
				<a class="carousel-item" href="#two!">
					<div class="card small horizontal card-border">
						<div class="card-stacked">

							<div class="card-content white-text"
								style="background-image: url('resources/image/6388.jpg'); border-radius: 20px;">
								<br>
										<h5 class="white-text" style="margin-top: 10%;">${testimonial.content}</h5>
										<h5 class="right-align">- ${testimonial.givenByName}</h5>
							</div>
						</div>
					</div>
				</a>
			</c:forEach>
			</c:if>
			

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--testimonial end-->
        
        <!--partners strat-->
        <div id="partnersDiv" class="sectionDiv">
            <br><br><br><br><br><br><br>
            <h3 class="center">Our Partners</h3>
            <!--PArtners carousel for large device starts -->
            <div class="row hide-on-med-and-down  aniview" data-av-animation="slideInUp" style="margin-top:5%;margin-bottom:5%;">

                <div class="col l1 m1 s1">
                    <a id="prevButton1" class="waves-effect waves-light" style="transform: translate(0,55px)">
                        <i class="medium material-icons black-text grey lighten-2">navigate_before</i></a>
                </div>
                <div class="col l10 m10 s10" id="partners">
                    <div class="carousel" style="overflow:hidden;">
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/allahabad.jpg');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/abhyudaya.jpg');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/axisBank.jpg');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/BOI.jpg');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal  white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/CentralBank.png');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal  white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/Citibank.png');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/CorporationBank.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/denaBank.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/hdfc.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/hsbc.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/icici.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/idbi.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/kotak.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/maharashtra.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/oriental.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/punjabNational.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/rbl.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/saraswat.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/sbi.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/svc.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/syndicate.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/theCooprative.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/union.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/YesBank.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col l1 m1 s1">
                    <a id="nextButton1" class="waves-effect waves-light" style="transform: translate(0,55px)">
                        <i class="medium material-icons black-text grey lighten-2">navigate_next</i></a>
                </div>
            </div>
            <!--Partners carousel for mobile device starts -->
            <div class="row hide-on-large-only  aniview" data-av-animation="slideInUp" style="margin-top:5%;margin-bottom:5%;">

                <div class="col l1 m6 s6">
                    <a id="prevBtn1" class="waves-effect waves-light" style="transform: translate(0,55px)">
                        <i class="medium material-icons black-text ">navigate_before</i></a>
                </div>
                <div class="col l1 m6 s6  ">
                    <a id="nextBtn1" class="waves-effect waves-light right" style="transform: translate(0,55px)">
                        <i class="medium material-icons black-text ">navigate_next</i></a>
                </div>

                <div class="col l10 m12 s12 " id="partners">
                    <div class="carousel" style="overflow:hidden;">
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/allahabad.jpg');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/abhyudaya.jpg');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/axisBank.jpg');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/BOI.jpg');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal  white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/CentralBank.png');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal  white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/Citibank.png');background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                        <br>

                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/CorporationBank.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/denaBank.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/hdfc.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/hsbc.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/icici.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">


                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/idbi.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/kotak.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/maharashtra.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/oriental.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/punjabNational.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/rbl.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/saraswat.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/sbi.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/svc.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/syndicate.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/theCooprative.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/union.png'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a class="carousel-item" href="#two!">
                            <div class="card small horizontal white">
                                <div class="card-stacked">
                                    <div class="card-content white-text" style="background-image: url('resources/image/bank/YesBank.jpg'); background-repeat:no-repeat; -webkit-filter: grayscale(100%);filter: grayscale(100%);">
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>

            </div>
        </div>
        <!--partners end-->

        <!--emi link start-->

        <div class="sectionDiv ">
            <br><br><br>
            <div class="row grey darken-3" id="emiDiv" style="">
                <div class="col s12 m6 l6  offset-l3 offset-m3 aniview center" style="margin-top:-5%; margin-right:5%;" data-av-animation="slideInUp">

                    <a href="emiPageForWebsite" class="center"><img src="resources/image/cal.jpg" class="circle" alt=""></a>


                    <a href="emiPageForWebsite" class="white-text">
                        <h3>Let us calculate your finance..</h3>
                        <!--<h2>Let's Calculate your Finance..</h2>-->
                    </a>

                </div>
            </div>
            <!--footer start-->
              <%@include file="components/footer.jsp"%>
               <!--footer end-->
        </div>
       
        <!--emi link end-->


		
        <!--action button strat-->
         <div class="fixed-action-btn">
            <a class="btn-floating btn-large black">
                <i class="large material-icons">view_list</i>
            </a>
            <ul>
                <li><a class="btn-floating red tooltipped" data-position="left" data-delay="50" data-tooltip="Emi Calculator" href="emiPageForWebsite"><i class="material-icons">insert_chart</i></a></li>
                <li><a class="btn-floating yellow darken-1 tooltipped" data-position="left" data-delay="50" data-tooltip="Contact Us" href="contactPageForWebsite"><i class="material-icons">perm_contact_calendar</i></a></li>
                <li><a class="btn-floating green modal-trigger tooltipped " id="modal" data-position="left" data-delay="50" data-tooltip="Call Me" href="#contactForm"><i class="material-icons">phone</i></a></li>

            </ul>
        </div>

       <!-- Modal Structure -->
    <div id="contactForm" class="modal modal-fixed-footer grey lighten-3">
     <form method="post" action="addingUserDetailsForWebsiteHomePageModal" name="contactForm">
        <div class="row modal-content ">
            <h4 class="center ">Let Us Contact You</h4>
            <hr>
            
               <div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">people</i>
					 <input id="userDetailsName" name="userDetailsName" type="text" class="validate text"  value="" title="Enter Valid Name">
					 <label for="userDetailsName">Name</label>
				</div>
				
			
			<div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">email</i>
					 <input id="emailId" name="emailId" type="email" class="validate"  value="" title="Enter Valid Email"> 
					 <label for="emailId">Email</label>
				</div>
				
				
				<div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">phone</i>
					 <input id="mobileNumber" name="mobileNumber" type="text" class="validate num"  value="" title="Enter Valid Mobile Number">
					  <label for="mobileNumber">Phone</label>
				</div>
				
				 
				<div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">redeem</i> 
					<select multiple name="serviceId" id="selectService" required  aria-required="true">
						<option value="" disabled>Choose your option</option>
						
						<c:if test="${not empty serviceList}">
						<c:forEach var="service" items="${serviceList}">
						
							<option value="<c:out value="${service.serviceId}" />"><c:out
									value="${service.serviceName}" /></option>
						</c:forEach>
						</c:if>
						
					</select> <label>Select Loan Type</label>
						
				</div>
		
		</div>

		<div class="row modal-footer grey lighten-3">
			<div class="col s6 m6 l6 offset-m1 offset-l1 offset-s3">
				<button type="submit" name="action" id="addUser"
					class="modal-action  btn waves-effect black">Submit</button>
			</div>
		</div>
        </form>
    </div>


        <!--action button end-->

    </main>
    <!--content End-->

    <div onclick="topFunction()" id="myBtn"><img src="resources/image/arrow-up-black.png" alt="scroll up" /></div>
    <!--<img src="resources/img/arrow-up.png" alt="scroll up" />-->
</body>

</html>
