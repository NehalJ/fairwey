<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  
    
    <%@include file="components/header_import.jsp"%>
   
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" >
     <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
    <style>
        label {
            color: white !important;
        }
        
        .input-field div.error {
            position: relative;
            top: -1rem;
            left: 15%;
            font-size: 12px;
            color: #FF4081;
            -webkit-transform: translateY(0%);
            -ms-transform: translateY(0%);
            -o-transform: translateY(0%);
            transform: translateY(0%);
        }
        #selectServiceForm .select-wrapper span.caret {
    color: white !important;
    position: absolute;
    right: 0;
    top: 0;
    bottom: 0;
    height: 10px;
    margin: 10px 0;
    font-size: 10px;
    line-height: 10px;
}
	#selectServiceForm select{
	border:1px solid red;
	}
	#iframeMed{
	height:300px;
	width:80%;
	}
    </style>

    <script>
   	$(document).ready(function(){
   		$(".dropdown-content li").click(function(){	 
            var a= $(".dropdown-content li").hasClass('active');
     		if(a === true){
     			
     			$("#serviceId-error").css("display","none");
     		}
     		else if(a === false)
     				 {
     			$("#serviceId-error").css("display","block");
     				 }
     		 
     	 });
   		
   	});
    </script>


</head>
<body>

<!--navbar start-->
		<%@include file="components/navbar.jsp"%>
	<!--navbar end-->


        
         <!--content start-->
        <div class="row grey lighten-2 " style="padding-top:2%;padding-bottom:3%;">

            <div class="col s12 m6 l6 offset-m1 offset-l1">
                <div class="card blue-grey darken-1 aniview" data-av-animation="slideInLeft">
                    <div class="card-content white-text" style="padding-right: 60px;">
                        <span class="card-title"><h4 class="left-align hide-on-small-only font-serif">Get In Touch</h4>
                            <h4 class="center-align hide-on-med-and-up font-serif">Get In Touch</h4>
                        </span>
                        <form method="post" action="addingUserDetailsForWebsiteContactUsPage" id="addUserFormFirst" name="contact" >
                            <div class="row">
                                <div class="input-field col s12 m8 l8">
                                    <i class="material-icons prefix">account_box</i>
                                    <input id="userDetailsName" type="text" name="userDetailsName" class="validate text" required>
                                    <label for="userDetailsName">Enter your name</label>
                                </div>
                                <div class="input-field col s12 m8 l8">
                                    <i class="material-icons prefix">phone_android</i>
                                    <input id="mobileNumber" type="text" class="validate num" name="mobileNumber" required>
                                    <label for="mobileNumber">Mobile Number</label>
                                </div>
                                                               
                                
                                <div class="input-field col s12 m8 l8">
                                    <i class="material-icons prefix">email</i>
                                    <input id="emailId" type="email" name="emailId" class="validate" required>
                                    <label for="emailId">Email Id</label>
                                </div>
                                
                                                               
                                
                                
                                <div class="input-field col s12 m8 l8" id="selectServiceForm">
									<i class="material-icons prefix">redeem</i>
					 
									<select multiple name="serviceId"  required >
										<option value="" disabled>Choose your option</option>
										<c:if test="${not empty serviceList}">
										<c:forEach var="service" items="${serviceList}">
										<option value="<c:out value="${service.serviceId}" />"><c:out value="${service.serviceName}" /></option>
									</c:forEach>
									</c:if>
						
									</select>	<label style="font-size:14px">Select Loan Type</label>
						
									</div>
                                
                           
                                
                                <%-- <div class="input-field col s12 m8 l8">
                                    <i class="material-icons prefix">redeem</i>
                                    <select id="serviceId" name="serviceId">
						<option value="" selected>Choose your option</option>
						
						<c:if test="${not empty serviceList}">
						<c:forEach var="service" items="${serviceList}">
						
							<option value="<c:out value="${service.serviceId}" />"><c:out
									value="${service.serviceName}" /></option>
						</c:forEach>
						</c:if>
                                    </select>
                                    <!--<label>Select Services Type</label>-->
                                </div> --%>
                                
                                
                                

                                <div class="input-field col s12 m8 l8">
                                    <i class="material-icons prefix">mode_edit</i>
                                    <textarea id="userDetailsMessage" class="materialize-textarea" name="userDetailsMessage" required></textarea>
                                    <label for="userDetailsMessage">Message</label>
                                </div>
                                <div class="input-field col s10 m11 l4 offset-l3 offset-m1 offset-s2">
                                    <button type="submit" name="action" id="addUserbtn" class="waves-effect waves-light btn white blue-grey-text">Submit<i class="material-icons right">send</i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l4" style="padding:0;">
                <div class="card cardCss hoverable hide-on-small-only aniview" data-av-animation="slideInRight">
                    <div class="card-content blue-grey-text">
                        <span class="card-title font-serif"><h3>Contact Us</h3></span>
                        <h6><a href="#mapDiv" class="blue-grey-text"><i class="material-icons small prefix blue-grey-text">location_on</i> Building no. 312/14, Ground Floor, Kalbadevi Road, Marine Lines,</a>
                        </h6>
                        <h6><a href="#mapDiv" class="blue-grey-text"> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Mumbai-400002 Landmark: Next to Kakad Market</a></h6>
                        
                        <h6><a class="blue-grey-text" href="mailto:akash@fairweyadvisory.com?Subject=Hello%20again" target="_top"><i class="material-icons prefix blue-grey-text">email</i> akash&#64;fairweyadvisory.com</a></h6>
                        <h6><a class="blue-grey-text" href="tel:022-40117406"><i class="material-icons prefix">phone</i> 022-40117406</a></h6>
                        <h6><a class="blue-grey-text" href="tel:+91-9167835957"><i class="material-icons prefix">phone_android</i> +91-9167835957</a></h6><br>

                        <!--<div class="row">
                        <div class="col l1 s3 m2 offset-l3 offset-m3">
                            <a href="#" target="_blank"> <i class="fa fa-facebook fa-2x blue-grey-text" aria-hidden="true"></i></a>
                        </div>
                        <div class="col l1 s3 m2">
                            <a href="#" target="_blank"><i class="fa fa-instagram fa-2x blue-grey-text" aria-hidden="true"></i></a></div>
                        <div class="col l1 s3 m2">
                            <a href="#" target="_blank"><i class="fa fa-linkedin fa-2x blue-grey-text" aria-hidden="true"></i></a></div>
                        <div class="col l1 s3 m2">
                            <a href="#" target="_blank"> <i class="fa fa-twitter fa-2x blue-grey-text" aria-hidden="true"></i></a>
                        </div>
                    </div>-->
                    </div>
                </div>
                <div class="card hoverable hide-on-med-and-up aniview" data-av-animation="slideInRight">
                    <div class="card-content">
                        <span class="card-title">Contact Us</span>
                        <p><i class="material-icons small prefix">location_on</i> Borivali East
                        </p>
                        <p><i class="material-icons prefix">email</i> f&#64;fairwey.com
                        </p>
                        <p><i class="material-icons prefix">phone_android</i> 123456789 </p>
                        <!--<div class="row">
                        <div class="col l1 s3 m2 offset-l3 offset-m3">
                            <a href="#" target="_blank"> <i class="fa fa-facebook fa-2x blue-grey-text" aria-hidden="true"></i></a>
                        </div>
                        <div class="col l1 s3 m2">
                            <a href="#" target="_blank"><i class="fa fa-instagram fa-2x blue-grey-text" aria-hidden="true"></i></a></div>
                        <div class="col l1 s3 m2">
                            <a href="#" target="_blank"><i class="fa fa-linkedin fa-2x blue-grey-text" aria-hidden="true"></i></a></div>
                        <div class="col l1 s3 m2">
                            <a href="#" target="_blank"> <i class="fa fa-twitter fa-2x blue-grey-text" aria-hidden="true"></i></a>
                        </div>
                    </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sectionDiv">
        <br> <br><br>
        <h4 class="center-align" style="font-family: serif;">Locate Us</h4>
       
        <div class="row grey lighten-4" style="margin-bottom:1px;" id="mapDiv">
            <div id="grad1" class="col l4 hide-on-med-and-down map-title" style="height:250px;">
                <br><br> <br><br> <br>
                <h3 class="center-align white-text" style="font-family: serif;width:400px; margin-top:2%;">Way to go Finance.. </h3>
            </div>
            <div class="col s12 m12 l8 hide-on-med-and-down" style="height:250px;">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d471.69793145954026!2d72.82835837602961!3d18.949823176634144!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7ce2272c3c5c1%3A0xa124b9d057c634d5!2sKakad+Market!5e0!3m2!1sen!2sin!4v1509438082929"
                    width="100%" height="450" style="border:0;height:250px;" frameborder="0" style="border:0" allowfullscreen></iframe>
                <!--<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15072.74626554913!2d72.974037!3d19.187052!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6c1290a2b76a2766!2sBlue+Square!5e0!3m2!1sen!2s!4v1506091755599" width="100%" height="450" frameborder="0"
                    style="border:0;height:250px;" allowfullscreen></iframe>-->
            </div>
            <div class="col s11 m11 offset-s1 offset-m1 hide-on-large-only">
                <iframe id="iframeMed" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d471.69793145954026!2d72.82835837602961!3d18.949823176634144!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7ce2272c3c5c1%3A0xa124b9d057c634d5!2sKakad+Market!5e0!3m2!1sen!2sin!4v1509438082929"
                    frameborder="0" style="border:0" allowfullscreen></iframe>
                <!--<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15072.74626554913!2d72.974037!3d19.187052!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6c1290a2b76a2766!2sBlue+Square!5e0!3m2!1sen!2s!4v1506091755599" frameborder="0" style="border:0" allowfullscreen></iframe>-->
            </div>
        </div>

        <!--content end-->
       	<%@include file="components/footer.jsp"%>
    </div>
    <!--footer end-->
    <!--action button strat-->
    <div class="fixed-action-btn">
        <a class="btn-floating btn-large black">
            <i class="large material-icons">view_list</i>
        </a>
        <ul>
            <li><a class="btn-floating red tooltipped" data-position="left" data-delay="50" data-tooltip="Emi Calculator" href="emiPageForWebsite"><i class="material-icons">insert_chart</i></a></li>
            <li><a class="btn-floating yellow darken-1 tooltipped" data-position="left" data-delay="50" data-tooltip="Contact Us" href="contactPageForWebsite"><i class="material-icons">perm_contact_calendar</i></a></li>
            <li><a class="btn-floating green modal-trigger tooltipped" data-position="left" data-delay="50" data-tooltip="Call Me" href="#contactForm"><i class="material-icons">phone</i></a></li>

        </ul>
    </div>


    <!-- Modal Structure -->
    <div id="contactForm" class="modal modal-fixed-footer grey lighten-3">
    <form method="post" action="addingUserDetailsForWebsiteContactUsPageModal" id="addUserForm" name="contactForm">
        <div class="row modal-content">
            <h4 class="center">Let Us Contact You</h4>
            <hr>
            
            <div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">people</i>
					 <input id="userDetailsName" name="userDetailsName" type="text" class="validate text" required value="" title="Enter Valid Name">
					 <label for="userDetailsName" class="black-text">Name</label>
				</div>
				
              <div class="input-field col s12 m6 l6 offset-l3 offset-m3">
                    <i class="material-icons prefix">email</i>
                    <input id="emailId" name="emailId" type="email" class="validate" required>
                    <label for="emailId" class="black-text">Email</label>
                </div> 
                
                <div class="input-field col s12 m6 l6 offset-l3 offset-m3">
                    <i class="material-icons prefix">phone</i>
                    <input id="mobileNumber" name="mobileNumber" type="text" class="validate num" required>
                    <label for="mobileNumber" class="black-text">Phone</label>
                </div>
                
                
                <div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">redeem</i> 
					<select multiple name="serviceId" required aria-required="true" class="materialSelect">
						<option value="" disabled>Choose your option</option>
						
						<c:if test="${not empty serviceList}">
						<c:forEach var="service" items="${serviceList}">
						
							<option value="<c:out value="${service.serviceId}" />"><c:out
									value="${service.serviceName}" /></option>
						</c:forEach>
						</c:if>
						
					</select> <label class="black-text">Select Loan Type</label>
						
				</div>
                
                
                
         
        </div>

        <div class="row modal-footer grey lighten-3">
            <div class="col s6 m6 l6 offset-m1 offset-l1 offset-s3">
                <button type="submit" name="action" id="addUser" href="#!" class="modal-action  btn waves-effect black">Submit</button>
            </div>
        </div>
        </form>
    </div>
	 <!-- modal for confirmation -->
     <div class="row">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="col s12 m12 l12">
					<div class="modal-content">
						
						<p id="msg"></p>
					
					</div>
					</div>
					
					<div class="modal-footer row">
					<div class="col s12 m2 l4 offset-l3">
					<a href="#" class="modal-action modal-close waves-effect waves-green blue btn">OK</a>
					<br>
					</div>
				</div>
			</div>
		</div>

    <!--action button end-->

    <div onclick="topFunction()" id="myBtn"><img src="resources/image/arrow-up-black.png" alt="scroll up" /></div>
</body>
</html>