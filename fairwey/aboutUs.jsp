<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    
    
    <%@include file="components/header_import.jsp"%>
    
    <script>
        
        //  add this function for initialization of scrollify
        $(function() {
            $.scrollify({
                section: ".example-classname", //give this class name to all div which is used as section
                // sectionName: "section-name",
                // interstitialSection: "",
                easing: "easeOutExpo",
                scrollSpeed: 1100,
                // offset: 0,
                scrollbars: true,
                // standardScrollElements: "",
                setHeights: true,
                overflowScroll: true,
                // updateHash: true,
                touchScroll: true,
                before: function(i, panels) {
                    var ref = panels[i].attr("data-section-name");
                    //for large device
                    $(".paginationLarge .active").removeClass("active");
                    //$(".pagination1 a ").css("color", "black");
                    $(".paginationLarge a ").html("<i class='material-icons tiny'>panorama_fish_eye</i>");
                    $(".paginationLarge").find("a[href=\"#" + ref + "\"]").addClass("active");

                    //for small device
                    $(".paginationSmall .active").removeClass("active");
                    //$(".pagination1 a ").css("color", "black");
                    $(".paginationSmall a").html("<i class='material-icons small'>panorama_fish_eye</i>");
                    $(".paginationSmall").find("a[href=\"#" + ref + "\"]").addClass("active");
                },

                afterRender: function() {
                    //   for large screen move on click of side indicators
                    $(".paginationLarge a").on("click", function() {
                        $.scrollify.move($(this).attr("href"));

                        if ($(this).hasClass('active')) {
                            $(this).html("<i class='material-icons tiny'>lens</i>");
                            // find("i").css("color", "red");
                        }
                    });
                    //   for small screen move on click of side indicators
                    $(".paginationSmall a").on("click", function() {
                        $.scrollify.move($(this).attr("href"));
                        if ($(this).hasClass('active')) {
                            $(this).html("<i class='material-icons small'>lens</i>");
                            // find("i").css("color", "red");
                        }
                    });
                    
                    
                    // you can also write this for move 
                    // $(".pagination1 a").on("click", $.scrollify.move);
                    //  $(this).css("color", "red");
                }

            });

		        });
        $(document).ready(function(){
        	$(".dropdown-content li").click(function(){	 
                var a= $(".dropdown-content li").hasClass('active');
         		if(a === true){
         			
         			$("#selectService-error").css("display","none");
         		}
         		else if(a === false)
         				 {
         			$("#selectService-error").css("display","block");
         				 }
         		 
         	 });
		});
    </script>
    <style>
               .fontBig {
            font-size: 60px !important;
        }
        
        .col .fixed-action-btn {
            position: fixed;
            right: -4%;
            bottom: 45%;
            padding-top: 15px;
            margin-bottom: 0;
            z-index: 998;
        }
        
        p,
        li h4,
        h3,
        h4,
        h5 {
            font-family: 'Open Sans', sans-serif;
            font-family: 'Ubuntu', sans-serif;
        }
        
        h3 {
            margin-bottom: 0;
            margin-top: 4px;
        }
        
        h4 {
            margin-top: 0;
        }
        
        .container {
            width: 90%;
        }
        
        .headerimg {
            margin-left: 13%;
        }
        
        .responsive-img {
            height: 64px !important;
        }
        
        @media only screen and (max-width: 992px) {
            .container {
                width: 80%;
            }
            .col .fixed-action-btn {
                position: fixed;
                right: -4%;
                bottom: 45%;
                padding-top: 15px;
                margin-bottom: 0;
                z-index: 998;
            }
            .headerimg {
                margin-left: 4%;
            }
        }
        
        @media only screen and (max-width: 600px) {
            .container {
                width: 90%;
            }
            .col .fixed-action-btn {
                position: fixed;
                right: 1%;
                bottom: 45%;
                padding-top: 15px;
                margin-bottom: 0;
                z-index: 998;
            }
            .headerimg {
                margin-left: 3%;
            }
        }
        /*h6 {
            font-size: 25px !important;
        }
        
        h6::first-letter {
            font-size: 200% !important;
            color: black;
        }*/
        
        #first {
            margin-left: 19%;
        }
        
        #second {
            margin-left: 40%;
        }
        
        #third {
            margin-left: 12%;
        }
        
        #abtcompany p {
            margin-top: 5px !important;
            margin-bottom: 5px !important;
        }
        
        @media only screen and (max-width: 992px) {
            #first {
                margin-left: 0 !important;
            }
            #second {
                margin-left: 30% !important;
            }
            #third {
                margin-left: 0 !important;
            }
        }
        
        @media only screen and (max-width: 600px) {
            #first {
                margin-left: 1% !important;
            }
            #second {
                margin-left: 16% !important;
            }
            #third {
                margin-left: 0 !important;
            }
        }
    </style>
</head>
<body>

	<!--navbar start-->
		<%@include file="components/navbar.jsp"%>
	<!--navbar end-->
	
	
	
	

<!--     navbar start background-color:black; 
    <nav class="nav-extended">
        <div class="nav-wrapper" style="min-height:45px; ">
            <span class="nav-title">Title</span>
            <ul class="right hide-on-med-and-down">
                <li><a>A link</a></li>
                <li><a>A second link</a></li>
                <li><a>A third link</a></li>
            </ul>
        </div>
        <div class="nav-content" style="height:60px;background-color:grey ;">
            <a href="#!" class="brand-logo">Logo</a>
            <ul class="right hide-on-med-and-down" style="margin-top:2%">
                <li><a>A link</a></li>
                <li><a>A second link</a></li>
                <li><a>A third link</a></li>
            </ul>
        </div>
    </nav>
    <div class="example-classname grey lighten-4" id="abtCompany" data-section-name="abtCompany">
        <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper">
                    <a href="homePageForWebsite" class="brand-logo">Fairwey</a>
                    <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <li><a href="homePageForWebsite">Home</a></li>
                        <li class="active"><a href="aboutUsPageForWebsite">About Us</a></li>
                        <li><a href="servicePageForWebsite">Services</a></li>
                        <li>
                            <a href="clientPageForWebsite">Clients</a>
                        </li>
                        <li><a href="emi.html">Emi Calculator</a></li>
                        <li><a href="contactPageForWebsite">Contact Us</a></li>
                        <li><a href="faqPageForWebsite">FAQ</a></li>
                    </ul>
                </div>
                <ul class="side-nav" id="mobile-demo">
                    <li><a href="homePageForWebsite">Home</a></li>
                    <li class="active"><a href="aboutUsPageForWebsite">About</a></li>
                    <li><a href="servicePageForWebsite">Services</a></li>
                    <li>
                        <a href="clientPageForWebsite">Clients</a>
                    </li>
                    <li><a href="emi.html">Emi Calculator</a></li>
                    <li><a href="contactPageForWebsite">Contact</a></li>
                    <li><a href="faqPageForWebsite">FAQ</a></li>

                </ul>
            </nav>
        </div>
        navbar end
         -->
         
        <!--content start-->
	
    <div class="example-classname grey lighten-4" id="abtCompany" data-section-name="abtCompany">
        <br><br><br><br>

        <!--<div class="example-classname grey lighten-4" id="abtCompany" data-section-name="abtCompany">-->
        <div class="container " id="abtcompany">
            <h4 class="center" style="margin-bottom:6px;">About Company </h4>
            <img class=" center responsive-img headerimg" src="resources/image/header2-01.png " alt=" ">
            <h5 class="center" style="margin-bottom:2px;margin-top:0;">WELCOME TO FAIRWEY ADVISORY!</h5>
            <h5 class="center" style="margin-bottom:2px;margin-top:2px;">"WAY TO GO FINANCE"</h5>
            <p>Fairwey Advisory is a proficient financial consulting firm that specializes in all kinds of advisory services related to business finance, mergers and acquisitions.</p>
            <p>Fairwey Advisory gives total financial solutions by identifying organization needs & satisfies the same by giving timely & cost effective solutions and caters services by organizing funds from local & international institutions both in Indian
                Rupees & foreign currency. While offering services mainly to SME and Large Corporates, the firm combines synergy of specialized competence and experience of professionals. The Company has been associated with the leading Banks & Institutions
                since its inception & have successfully completed number of assignments by Raising of Term Loan, Working Capital Finance & Structured Finance for both SME's & Corporates, our Prestigious Clientele include Business people, Professionals
                and Executives of Public and Private Sector Companies as well as MNCs spread across Geographical boundaries and Industry Sectors.</p>
            <p>
                Our associates deal in various types of financial services required by an individual or a Company. We arrange an end to end support services for the financial help which is required by customers.

            </p>
            <p>
                Our perfect blend of experience and youth helps us stay dynamic and keeps us acquainted with the changing market trends. Our team comprising CAs & ex-bankers who have rich experience, plus our exuberant youth together mould their efforts and energies
                for the betterment and development of our esteemed clients.

            </p>
            <p>
                We help you overcome challenges and catch hold of unique opportunities as and when they arise. We are with you every step of the way, right from selecting the right financing instrument, the right financier and getting it at the right cost.

            </p>
            <p>
                There are many other ways in which an innovative company as ours can help clients run their business and their personal financial affairs more efficiently so they have more time to concentrate on their core business activities. Through our services, support
                and advice, we aim to make a real contribution to the profitability of every client's business and their personal financial well-being. At the same time, we want to take the pressure off them, making their life a little bit simpler and
                easier.
            </p>
            <i class="material-icons" id="first">format_quote</i>
            <h5 class="center-align" style="margin-bottom:2px;margin-top:0;">

                <span class="fontBig">T</span>he best preparation for tomorrow is doing your best today.<i class="material-icons">format_quote</i>

            </h5>


            <h6 class="center-align"></h6>
        </div>
    </div>

    <div class="example-classname" id="abtPromoter" data-section-name="abtPromoter">
        <div class="container ">
            <br> <br> <br> <br> <br>
            <h4 class="center">About Promoter</h4>

            <img class=" center responsive-img headerimg" src="resources/image/header2-01.png " height="64px;">

            <div class="row">
                <div class="col s12 l4 m4">
                    <img src="resources/image/man.jpg" alt="Promoter" width="100%;">
                </div>
                <div class="col s12 l8 m8">
                    <p>
                        Akash Tiwari is Fairwey Advisory's Promoter. He has strong strategic and tactical management skills and combines them with a passion to develop services that make a positive difference to Clients.

                    </p>
                    <p>
                        Prior to incorporating Fairwey Advisory, Akash was an ex-banker and has also worked with few financial advisors. He has brought together several years of experience in loan syndication industry. Akash has also travelled extensively for his post graduation
                        in Msc. In Financial Economics and Business Analysis and also has experience of working in the UK gives him an edge. He looks after Working Capital Products like Cash Credit, Overdraft, Term loan & Trade Finance and much more.
                        He helps in preparing the credit approval memo (Information of Memorandum of the firm, Financial (CMA) Data & Brief details about the future prospect / project). His excellent people management skills helps the Company retain its
                        skilled staff over the years.

                    </p>
                </div>
            </div>

            <br>

            <!--<i class="fa fa-quote-left" aria-hidden="true"></i>-->
            <i class="material-icons" id="second">format_quote</i>
            <h5 class="center-align" style="margin-top:0">

                <span class="fontBig">I</span> do what I do.<i class="material-icons">format_quote</i>

            </h5>


            <!--<i class="fa fa-quote-right" aria-hidden="true"></i>-->
            <!--<span class="center"> Raghuram Rajan</span>-->
        </div>
    </div>
    <div class="example-classname grey lighten-4" id="ourVision" data-section-name="ourVision">
        <div class="container ">
            <br> <br> <br> <br> <br>
            <h4 class="center">About Vision</h4>

            <img class=" center responsive-img headerimg" src="resources/image/header2-01.png " alt=" ">

            <p>
                "To be an organization known across the globe as a best financial consulting firm for its reliability, responsibility and finesse."
            </p>
            <h4 class="center">Our Mission </h4>

            <p>
                Provide clients with meaningful and dependable solutions and maintain highest ethical standards on integrity, transparency and confidentiality in our business dealings
            </p>
            <h4 class="center">Our Culture</h4>

            <p>
                We have strongly adopted culture and values in our organization. Our belief in teamwork, transparency, punctuality, and dedication in work has encouraged us to grow and will continue to expand in future.
            </p>
            <br>
            <i class="material-icons" id="third">format_quote</i>
            <h5 class="center-align" style="margin-top:0">

                <span class="fontBig">D</span>o not save what is left after spending, but spend what is left after saving.<i class="material-icons">format_quote</i>

            </h5>
        </div>
    </div>
    <div class="example-classname " id="whyUs" data-section-name="whyUs">
        <div class="container ">
            <br> <br> <br>
            <!--<br> <br> <br>-->
            <h4 class="center " style="margin-bottom:0;">Why Us?</h4>
            <!--<h5 class="center hide-on-med-and-up">Why Us?</h5> hide-on-small-only-->
            <img class=" center responsive-img headerimg" src="resources/image/header2-01.png " alt=" ">
            <div class="row" style="margin-bottom:0px;">
                <!--<br> <br> <br>-->
                <div class="col s12 m12 l6 hide-on-med-and-down">
                    <ul>
                        <li>
                            <h5> <i class="material-icons prefix">chevron_right</i> Efficient and timely service</h5>
                        </li>
                        <li>
                            <h5> <i class="material-icons prefix">chevron_right</i>Reasonable Advice</h5>
                        </li>
                        <li>
                            <h5> <i class="material-icons prefix">chevron_right</i>Customer Centricity</h5>
                        </li>
                        <li>
                            <h5> <i class="material-icons prefix">chevron_right</i>Experienced and qualified team</h5>
                        </li>
                        <li>
                            <h5> <i class="material-icons prefix">chevron_right</i>Reliable Service</h5>
                        </li>

                    </ul>
                </div>
                <div class="col s12 m12 l6 hide-on-med-and-down">
                    <ul>
                        <li>
                            <h5> <i class="material-icons prefix">chevron_right</i>Documentation support</h5>
                        </li>
                        <li>
                            <h5> <i class="material-icons prefix">chevron_right</i>Most Comprehensive Planning</h5>
                        </li>
                        <li>
                            <h5> <i class="material-icons prefix">chevron_right</i>Committed & Accountable</h5>
                        </li>
                        <li>
                            <h5> <i class="material-icons prefix">chevron_right</i>Charge on Success of Service</h5>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col s12 m12 l6 hide-on-large-only">
                <ul>
                    <li>
                        <h6 style="font-size: 1.5rem;"> <i class="material-icons prefix">chevron_right</i> Efficient and timely service</h6>
                    </li>
                    <li>
                        <h6 style="font-size: 1.5rem;"> <i class="material-icons prefix">chevron_right</i>Reasonable Advice</h6>
                    </li>
                    <li>
                        <h6 style="font-size: 1.5rem;"> <i class="material-icons prefix">chevron_right</i>Customer Centricity</h6>
                    </li>
                    <li>
                        <h6 style="font-size: 1.5rem;"> <i class="material-icons prefix">chevron_right</i>Experienced and qualified team</h6>
                    </li>
                    <li>
                        <h6 style="font-size: 1.5rem;"> <i class="material-icons prefix">chevron_right</i>Reliable Service</h6>
                    </li>

                </ul>
            </div>
            <div class="col s12 m12 l6 hide-on-large-only">
                <ul>
                    <li>
                        <h6 style="font-size: 1.5rem;"> <i class="material-icons prefix">chevron_right</i>Documentation support</h6>
                    </li>
                    <li>
                        <h6 style="font-size: 1.5rem;"> <i class="material-icons prefix">chevron_right</i>Most Comprehensive Planning</h6>
                    </li>
                    <li>
                        <h6 style="font-size: 1.5rem;"> <i class="material-icons prefix">chevron_right</i>Committed & Accountable</h6>
                    </li>
                    <li>
                        <h6 style="font-size: 1.5rem;"> <i class="material-icons prefix">chevron_right</i>Charge on Success of Service</h6>
                    </li>
                </ul>
            </div>
        </div>
        <!--footer start-->
   		  <%@include file="components/footer.jsp"%>
   		<!--footer end-->
    </div>
    
    <!--action button strat-->
    <div class="fixed-action-btn">
        <a class="btn-floating btn-large black">
            <i class="large material-icons">view_list</i>
        </a>
        <ul>
            <li><a class="btn-floating red tooltipped" data-position="left" data-delay="50" data-tooltip="Emi Calculator" href="emiPageForWebsite"><i class="material-icons">insert_chart</i></a></li>
            <li><a class="btn-floating yellow darken-1 tooltipped" data-position="left" data-delay="50" data-tooltip="Contact us" href="contactPageForWebsite"><i class="material-icons">perm_contact_calendar</i></a></li>
            <li><a id="modal" class="btn-floating green modal-trigger tooltipped" data-position="left" data-delay="50" data-tooltip="Call Me" href="#contactForm"><i class="material-icons">phone</i></a></li>

        </ul>
    </div>


    <!-- Modal Structure -->
    <div id="contactForm" class="modal modal-fixed-footer grey lighten-3">
    <form method="post" action="addingUserDetailsForWebsiteAboutUsPageModal" id="addUserForm" name="contactForm">
        <div class="row modal-content">
            <h4 class="center">Let Us Contact You</h4>
            <hr>
                     <div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">people</i>
					 <input id="userDetailsName" name="userDetailsName" type="text" class="validate text" required value="" title="Enter Valid Name">
					 <label for="userDetailsName">Name</label>
				</div>
				
			
				 <div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">email</i>
					 <input id="emailId" name="emailId" type="email" class="validate" required value="" title="Enter Valid Email"> 
					 <label for="emailId">Email</label>
				</div> 
				
				
				<div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">phone</i>
					 <input id="mobileNumber" name="mobileNumber" type="text" class="validate num" required value="" title="Enter Valid Mobile Number">
					  <label for="mobileNumber">Phone</label>
				</div>
				
				 
				<div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">redeem</i> 
					<select multiple name="serviceId" id="selectService" required aria-required="true">
						<option value="" disabled>Choose your option</option>
						
						<c:if test="${not empty serviceList}">
						<c:forEach var="service" items="${serviceList}">
						
							<option value="<c:out value="${service.serviceId}" />"><c:out
									value="${service.serviceName}" /></option>
						</c:forEach>
						</c:if>
						
					</select> <label>Select Loan Type</label>
						
				</div>
				
		</div>

		<div class="row modal-footer grey lighten-3">
			<div class="col s6 m6 l6 offset-m1 offset-l1 offset-s3">
				<button type="submit" name="action" id="addUser"
					class="modal-action  btn waves-effect black">Submit</button>
			</div>
		</div>
        </form>
    </div>


    <!--action button end-->
    <!--content end-->



    <!--side Indicators for large screen -->
    <div class="col s12 m3 l2 hide-on-med-and-down">
        <ul class="fixed-action-btn paginationLarge" style="right:5%;">
            <li>
                <a href="#abtCompany" class="active tooltipped" data-position="left" data-delay="10" data-tooltip="About Company"><i class="material-icons tiny">panorama_fish_eye</i></a>

            </li>
            <li>
                <a href="#abtPromoter" id="a2" class="tooltipped" data-position="left" data-delay="10" data-tooltip="About Promoter"><i class="material-icons tiny">panorama_fish_eye</i></a>
            </li>
            <li>
                <a href="#ourVision" id="a3" class="tooltipped" data-position="left" data-delay="10" data-tooltip="About Vision"><i class="material-icons tiny">panorama_fish_eye</i></a>
            </li>
            <li>
                <a href="#whyUs" id="a4" class="tooltipped" data-position="left" data-delay="10" data-tooltip="Why Us?"><i class="material-icons tiny">panorama_fish_eye</i></a>
            </li>

        </ul>
    </div>
    <!--side Indicators for small screen -->
    <div class="col s12  m3 l2 hide-on-large-only show-on-medium">
        <ul class="fixed-action-btn paginationSmall">
            <li>
                <!--<a href="#div1" class="active tooltipped" data-position="left" data-delay="50" data-tooltip="div1"><i class="material-icons large" >accessibility</i></a>-->
                <a href="#abtCompany" class="active tooltipped" data-position="left" data-delay="50" data-tooltip="About Company"><i class="material-icons small">panorama_fish_eye</i></a>
            </li>
            <li>
                <a href="#abtPromoter" class="tooltipped" data-position="left" data-delay="50" data-tooltip="About Promoter"><i class="material-icons small">panorama_fish_eye</i></a>
            </li>
            <li>
                <a href="#ourVision" class="tooltipped" data-position="left" data-delay="50" data-tooltip="About Vision"><i class="material-icons small">panorama_fish_eye</i></a>
            </li>
            <li>
                <a href="#whyUs" class="tooltipped" data-position="left" data-delay="50" data-tooltip="Why Us?"><i class="material-icons small">panorama_fish_eye</i></a>
            </li>
        </ul>
    </div>
    <div onclick="topFunction()" id="myBtn"><img src="resources/image/arrow-up-black.png" alt="scroll up" /></div>
   
</body>
</html>