 $(document).ready(function() {
     $(".button-collapse").sideNav();
     // hides and shows side nav on click of button
     $('#slide').click(function() {
         $(this).toggleClass("sidebutton");
         // hides all heading and shows only icons
         $('#logo-collapse').toggle();
         $('#logo-collapse-full').toggle();
         $('.element-sideNav').toggle();

         //hide logo 
         // $('.logo').toggle();
         // half closes side nav
         $('.side-nav').toggleClass("reveal-open");
         // remove left padding of body
         $('main').toggleClass("paddingBody");
         // starts body content after half side nav ends
         $('main').toggleClass("intro");
     });


     $('.collapsible').collapsible();
     $('select').material_select();
     $('.modal').modal();

     //  for no of entries and global search
     $('#tblData').DataTable({
         "oLanguage": {
             "sLengthMenu": "Display Records _MENU_"
         },
         lengthMenu: [
             [10, 25., 50, -1],
             ['10 ', '25 ', '50 ', 'all']
         ],
         //  scrollX: true,
         dom: 'lBfrtip',
         buttons: {
             buttons: [
                 //      {
                 //      extend: 'pageLength',
                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
                 //  }, 
                 {
                     extend: 'pdf',
                     className: 'pdfButton waves-effect waves-light   white-text blue-grey lighten-1 hoverable',
                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
                     //title of the page
                     title: function() {
                         var name = $(".heading h4").text();
                         return name
                     },
                     //file name 
                     filename: function() {
                         var d = new Date();
                         var date = d.getDate();
                         var month = d.getMonth();
                         var year = d.getFullYear();
                         var name = $(".heading h4").text();
                         return name + date + '-' + month + '-' + year;
                     },
                     //  exports only dataColumn
                     exportOptions: {
                         columns: ':visible.print-col'
                     },
                 },
                 {
                     extend: 'excel',
                     className: 'excelButton waves-effect waves-light   white-text blue-grey lighten-1 hoverable',
                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
                     //title of the page
                     title: function() {
                         var name = $(".heading h4").text();
                         return name
                     },
                     //file name 
                     filename: function() {
                         var d = new Date();
                         var date = d.getDate();
                         var month = d.getMonth();
                         var year = d.getFullYear();
                         var name = $(".heading h4").text();
                         return name + date + '-' + month + '-' + year;
                     },
                     //  exports only dataColumn
                     exportOptions: {
                         columns: ':visible.print-col'
                     },
                 },
                 {
                     extend: 'print',
                     className: 'printButton waves-effect waves-light   white-text blue-grey lighten-1 hoverable',
                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
                     //title of the page
                     title: function() {
                         var name = $(".heading h4").text();
                         return name
                     },
                     //file name 
                     filename: function() {
                         var d = new Date();
                         var date = d.getDate();
                         var month = d.getMonth();
                         var year = d.getFullYear();
                         var name = $(".heading h4").text();
                         return name + date + '-' + month + '-' + year;
                     },
                     //  exports only dataColumn
                     exportOptions: {
                         columns: ':visible.print-col'
                     },
                 },
                 {
                     extend: 'colvis',
                     className: 'colvisButton waves-effect waves-light  white-text blue-grey lighten-1 hoverable',
                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
                     collectionLayout: 'fixed two-column',
                     align: 'left'
                 },
             ]
         }

     });
     //  $('.checkAll').on('click', function() {
     //      $(this).closest('table').find('tbody :checkbox')
     //          .prop('checked', this.checked)
     //          .closest('tr').toggleClass('selected', this.checked);
     //  });

     //  $('tbody :checkbox').on('click', function() {
     //      $(this).closest('tr').toggleClass('selected', this.checked);

     //      $(this).closest('table').find('.checkAll').prop('checked', ($(this).closest('table').find('tbody :checkbox:checked').length == $(this).closest('table').find('tbody :checkbox').length)); //Tira / coloca a seleção no .checkAll
     //  });


     //for add placeholder in dataTable search 
     $('.dataTables_filter input').attr("placeholder", "       search");
     $('.datepicker').pickadate({

         selectMonths: true, // Creates a dropdown to control month
         selectYears: 15, // Creates a dropdown of 15 years to control year,
         today: 'Today',
         clear: 'Clear',
         close: 'Ok',
         format: 'yyyy-mm-dd',
         closeOnSelect: 'true', // Close upon selecting a date,
         // this function closes after date selection Close upon
         // selecting a date,
         onSet: function(ele) {
             if (ele.select) {
                 this.close();
             }
         }

     });

     // show hide Range dates
     $(".showDates").hide();
     $(".rangeSelect").click(function() {
         $(".showDates").show();
     });
     $(".showDates1").hide();
     $(".rangeSelect1").click(function() {
         $(".showDates1").show();
     });

     $('select').material_select();
     $("select")
         .change(function() {
             var t = this;
             var content = $(this).siblings('ul').detach();
             setTimeout(function() {
                 $(t).parent().append(content);
                 $("select").material_select();
             }, 200);
         });
     //  $(".status").click(function() {

     //      if (confirm("Are you sure you want to change status?")) {
     //          ($(this).text() === "Active") ? $(this).text("Inactive").css("color", "red"): $(this).text("Active").css("color", "green");
     //      } else {
     //          return false;
     //      }

     //  });
     //  $(".membership").click(function() {
     //      if (confirm("Are you sure you want to change Membership?")) {
     //          ($(this).text() === "General") ? $(this).text("Featured").css("color", "#ff6f00"): $(this).text("General").css("color", "blue");
     //      } else {
     //          return false;
     //      }
     //  });


     //     $('#text1').trigger('autoresize');

 });