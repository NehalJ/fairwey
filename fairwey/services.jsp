<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
        
    <%@include file="components/header_import.jsp"%>    
    <script>
    $(document).ready(function() {
    
       /*  $(function() { */
$('.modal').modal({
    	// Callback for Modal open. Modal and trigger parameters available.
        	 ready: function(modal, trigger) { 
    	        
    	        stopScrollify();
    	      },
    	    
    	   // Callback for Modal close
    	 complete: function() {startSrollify();
    	 }
     });
            if ($(window).width() >= 770) {
            	
                $.scrollify({
                    section: ".sectionDiv",
                    sectionName: ".sectionDiv",
                    easing: "easeOutExpo",
                    scrollSpeed: 1100,
                    offset: 0,
                    scrollbars: true,
                    before: function() {},
                    after: function() {},
                    afterResize: function() {}
                });
            }
        /* }); */
        // $(window).scroll(function(e) {
        //     alert();
        //     e.preventDefault();
        // });
        $('.parallax').parallax();
        // $(".divsnip1482").hide();
        $("#divsnip1482test1").hide();
        $("#divsnip1482test2").hide();
        $("#divbuyerCredit").hide();
        $("#divexportBill").hide();
        $("#divbankGuarantee").hide();
        $("#divloanAgainstprop").hide();
        $("#divunsecuredBusinessLoan").hide();
        $("#divleaseRentalDiscount").hide();
        $("#divloanAgainstSecurity").hide();


        $("#loanAgainstprop").click(function() {
            $("#divunsecuredBusinessLoan").hide();
            $("#divloanAgainstprop").toggle();
        });

        $("#unsecuredBusinessLoan").click(function() {
            $("#divloanAgainstprop").hide();
            $("#divunsecuredBusinessLoan").toggle();
        });
        $("#leaseRentalDiscountbtn").click(function() {
            $("#divloanAgainstSecurity").hide();
            $("#divleaseRentalDiscount").toggle();
        });
        $("#loanAgainstSecurity").click(function() {
            $("#divleaseRentalDiscount").hide();
            $("#divloanAgainstSecurity").toggle();

        });
        $("#test1").click(function() {
            $("#divsnip1482test2").hide();
            $("#divsnip1482test1").toggle();
        });
        $("#test2").click(function() {
            $("#divsnip1482test1").hide();
            $("#divsnip1482test2").toggle();
        });

        $("#buyerCredit").click(function() {
            $("#divexportBill").hide();
            $("#divbuyerCredit").toggle();
        });
        $("#exportBill").click(function() {
            $("#divbuyerCredit").hide();
            $("#divexportBill").toggle();
        });

        $("#bankGuarantee").click(function() {
            $("#divbankGuarantee").toggle();
        });
        $(".dropdown-content li").click(function(){	 
            var a= $(".dropdown-content li").hasClass('active');
     		if(a === true){
     			
     			$("#selectService-error").css("display","none");
     		}
     		else if(a === false)
     				 {
     			$("#selectService-error").css("display","block");
     				 }
     		 
     	 });
    });
    </script>
    <style>
     body {
            font-size: 14px !important;
            /*for sticky footer*/
            display: block;
        }
        
        h6 {
            font-size: 0.83rem;
            line-height: 110%;
            margin: .5rem 0 .4rem 0;
        }
        
        .row {
            margin-left: auto;
            margin-right: auto;
        } 
        
        footer .row {
            height: 205px !important;
        }
        
        .ourApproach .parallax-container {
            height: 315px;
        }
        
        ul li {
            font-size: 14px;
        }
        
        @media only screen and (max-width:992px) {
            footer .row {
                height: 100% !important;
            }
            .ourApproach .parallax-container {
                height: 430px;
            }
            ul li {
                font-size: 12px;
            }
           
        }
        
        @media only screen and (max-width:600px) {
          /*   .row {
                min-height: 600px;
            } */
            footer .row {
                height: 80% !important;
            }
            .ourApproach {
                height: 600px !important;
            }
            .responsive-img {
                /*position: relative;*/
            }
           
            .ourApproach .parallax-container {
                height: 590px;
            }
            ul li {
                font-size: 10px;
            }
        }
        /*for features in mobile*/
        
        @media only screen and (min-width:993px) {}
        
        .black darken-5 {
            background-color: #1a1a1a !important;
        }
        
        .frame {
            position: relative;
            /*display: inline-block;*/
            overflow: auto;
        }
          </style>

</head>
<body>

	<!--navbar start-->
		<%@include file="components/navbar.jsp"%>
	<!--navbar end-->
   
 <!--content start-->


    <!--CORPORATE FINANCE start-->


    <!--structured finance  start-->

    <div class="row sectionDiv" id="structuredFinance">
        <!--large device-->
        <div class="hide-on-med-and-down ">
            <div class="parallax-container " style="height:660px; ">
                <div class="parallax "> <img class="responsive-img" src="resources/image/structure2.jpg" alt="sample45" />
                    <figcaption class="blur">
                        <h3>Structured Finance</h3>
                        <p>Structured finance is a highly involved financial instrument offered to large financial institutions or companies that have complex financing needs that don't match with conventional financial products.
                            <ul>
                                <li><i class="material-icons left tiny">fiber_manual_record</i> Collateralize mortgage obligations (CMOs)</li>
                                <li><i class="material-icons left tiny">fiber_manual_record</i> Collateralized debt obligations (CDOs) </li>
                                <li><i class="material-icons left tiny">fiber_manual_record</i> Collateralize bond obligations (CBOs) </li>
                                <li><i class="material-icons left tiny">fiber_manual_record</i> Credit default swaps (CDSs), </li>
                                <li><i class="material-icons left tiny">fiber_manual_record</i> Hybrid securities combining elements of debt and equity securities.</li>
                            </ul>
                        </p>
                    </figcaption>
                </div>
            </div>
        </div>
        <!--small device-->

        <div class="col s12 m12 l12 hide-on-large-only">
            <br>
            <img class="responsive-img" src="resources/image/structure2.jpg" alt="sample45" />

            <figcaption class="blur">
                <h3>Structured Finance</h3>
                <p>Structured finance is a highly involved financial instrument offered to large financial institutions or companies that have complex financing needs that don't match with conventional financial products. </p>
                    <ul>
                        <li><i class="material-icons left tiny">fiber_manual_record</i> Collateralize mortgage obligations (CMOs)</li>
                        <li><i class="material-icons left tiny">fiber_manual_record</i> Collateralized debt obligations (CDOs) </li>
                        <li><i class="material-icons left tiny">fiber_manual_record</i> Collateralize bond obligations (CBOs) </li>
                        <li><i class="material-icons left tiny">fiber_manual_record</i> Credit default swaps (CDSs), </li>
                        <li><i class="material-icons left tiny">fiber_manual_record</i> Hybrid securities combining elements of debt and equity securities.</li>
                    </ul>
               
            </figcaption>
            <br>
        </div>
    </div>
    
    <!--structured finance  end-->
    <!--letter of credit and cash credit start-->
    <div class="row sectionDiv" id="letterofCredit">
        <!--for large device-->
        <!--for large device-->
        <div class="hide-on-med-and-down"> <br class="hide-on-med-and-down"><br class="hide-on-med-and-down"><br class="hide-on-med-and-down"><br class="hide-on-med-and-down"><br class="hide-on-med-and-down"><br class="hide-on-med-and-down">

            <div class="col s12 m6 l6  left aniview" data-av-animation="slideInLeft" style="padding-left:0;">
                <figure class="snip1482" style="margin-left:1%;">
                    <figcaption class="center">
                        <h6>Letter of Credit</h6>
                        <hr>
                        <p>It is a facility given by the bank to companies, to withdraw money "more " than the balance available in their respective accounts. It is granted against assets like financial instrument and property.<br><br>
                        </p>
                        <ul class="left-align">
                            <li><i class="material-icons left tiny ">fiber_manual_record</i> ROI from 8.5% onwards </li>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i> Processing time-1 day</li><br><br><br>
                        </ul>
                    </figcaption>
                    <img class="responsive-img " src="resources/image/letterofcredit3.jpg ">
                </figure>
            </div>
            
            <div class="col s12 m6 l6 hide-on-med-and-down right aniview" data-av-animation="slideInRight" style="padding-left:0;">
                <figure class="snip1482" style="margin-left:1%;">
                    <figcaption class="center">
                        <h6>Cash Credit (CC)</h6>
                        <hr>
                        <p class="flow-text">It is a type of short term loan provided to companies to fulfill their working capital requirement. It is granted against hypothecation of stock and assets such as raw materials, work-in process, finished goods and stock-in-trade,
                            including stores and spares. </p>
                        <br>  <br>
                        <ul class="left-align">
                            <li><i class="material-icons left tiny">fiber_manual_record</i>ROI from 9% onwards </li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>As low as 25% Collateral</li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>Processing time-45 days</li>



                        </ul>

                    </figcaption>
                    <img class="responsive-img" src="resources/image/CashCredit.jpg" alt="sample45" />
                </figure>
            </div>
        </div>
        <!--for small device-->
        <div class="col s12 m9 l6 offset-m3  aniview hide-on-large-only" data-av-animation="slideInLeft">
            <br><br><br><br>
            <figure class="snip1482">
                <figcaption>
                    <h6>Letter of Credit</h6>
                    <hr>
                    <p class="center">It is a facility given by the bank to companies, to withdraw money "more " than the balance available in their respective accounts.
                        <button class="btn-flat white-text" style="font-size:0.8em" id="test1">Read more</button>
                    </p>
                </figcaption>
                <img class="responsive-img " src="resources/image/letterofcredit3.jpg ">
            </figure>
            <figure class="snip1482" id="divsnip1482test1">
                <figcaption style="width:100%">
                    <p class="center">
                        It is granted against assets like financial instrument and property.</p>
                        <ul class="left-align">
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>ROI from 8.5% onwards </li>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Processing time-1 day</li>
                        </ul>
                    
                </figcaption>
            </figure>
            <!--<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/sample45.jpg" alt="sample45" /></figure>-->
        </div>
        <div class="col s12 m9 l6 offset-m3  aniview hide-on-large-only " data-av-animation="slideInRight">
            <figure class="snip1482">
                <figcaption>
                    <h6>Cash Credit (CC)</h6>
                    <hr>
                    <p class="flow-text center">It is a type of short term loan provided to companies to fulfill their working capital requirement. </p>

                    <button class="btn-flat white-text" style="font-size:0.8em" id="test2">Read more</button>
                </figcaption>
                <img src="resources/image/CashCredit.jpg" alt="sample45" /></figure>
            <figure class="snip1482" id="divsnip1482test2" style="margin-top:0">
                <figcaption style="width:100%">
                    <p class="flow-text center">It is granted against hypothecation of stock and assets such as raw materials, work-in process, finished goods and stock-in-trade,
                            including stores and spares.
                    </p>
                    <ul class="left-align">
                            <li><i class="material-icons left tiny">fiber_manual_record</i>ROI from 9% onwards </li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>As low as 25% Collateral</li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>Processing time-45 days</li>



                        </ul>
                </figcaption>
            </figure>
        </div>
    </div>
   
    <!--letter of credit and cash credit end-->

    <!--cgtmse start-->
    <div class="row sectionDiv" id="cgtmse">
        <!--for large device-->
        <div class="hide-on-med-and-down">
            <div class="parallax-container " style="height:660px; ">
                <div class="parallax "> <img class="responsive-img" src="resources/image/cgtmse1.jpg" alt="sample45" />
                    <figcaption class="blur ">
                        <h3>CGTMSE</h3>
                        <h5>CREDIT GUARANTEE FUND SCHEME FOR MICRO AND SMALL ENTERPRISES </h5>
                        <p>We can arrange unsecured CC,OD & Term Loan up to 200 lacs for new and existing manufacturing companies under the govt. scheme
                        </p>
                    </figcaption>
                </div>
            </div>
        </div>
        <!--for small device-->
        <div class="col s12 m12 l12 hide-on-large-only">
            <img class="responsive-img" src="resources/image/cgtmse1.jpg" alt="sample45" />
            <figcaption class="blur">
                <h3>CGTMSE</h3>
                <h5>CREDIT GUARANTEE FUND SCHEME FOR MICRO AND SMALL ENTERPRISES </h5>
                <p>We can arrange unsecured CC,OD & Term Loan up to 200 lacs for new and existing manufacturing companies under the govt. scheme
                </p>
            </figcaption>
            <br>
        </div>
    </div>
    
    <!--cgtmse end-->


    <!--project finance start-->
    <div class="row sectionDiv" id="projectFinance">
        <!--for large device-->
        <div class=" hide-on-med-and-down ">
            <br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down ">
            <div class="col s12 m12 l6 aniview " data-av-animation="slideInLeft ">
                <div class="zoomin frame ">
                    <img src="resources/image/ProjectFinance.jpg " alt=" " style="cursor:pointer; ">
                </div>
            </div>
            <div class="col m12 l6 s12 aniview " data-av-animation="slideInRight ">
                <div class="content card ">
                    <div class="card-content ">
                        <h5>Project Finance(Infrastructure / Non Infrastructure sectors) </h5>
                        <hr>
                        <p>Project finance is the financing of long-term infrastructure, industrial projects and public services based on a non-recourse or limited recourse financial structure, in which project debt and equity used to finance the project
                            are paid back from the cash flow generated by the project. </p>
                        <p>It is a financial service for new projects as well as expansion, diversification and modernization of existing projects in infrastructure and non -infrastructure sectors</p>
                        <br>
                        <h6><i class="material-icons left tiny ">fiber_manual_record</i>Foreign currency term loans</h6>
                        <h6><i class="material-icons left tiny ">fiber_manual_record</i>External Commercial Borrowings</h6>
                        <h6><i class="material-icons left tiny ">fiber_manual_record</i>Export Credit Agency backed long-term debt</h6>
                        <h6><i class="material-icons left tiny ">fiber_manual_record</i>Lines of credit from different multilateral institutions</h6>
                        <h6><i class="material-icons left tiny ">fiber_manual_record</i>Non-fund based facilities like Letter of Credit, Bank Guarantee, Supplier's Credit, Buyer's Credit etc
                        </h6>
                        <h6><i class="material-icons left tiny ">fiber_manual_record</i>Subordinated debt and mezzanine financing.</h6>
                    </div>
                </div>
            </div>
        </div>
        <!--for small device-->
        <div class="hide-on-large-only">
            <div class="col s12 m12 l6 aniview " data-av-animation="slideInLeft ">
                <div class="zoomin frame ">
                    <img src="resources/image/ProjectFinance.jpg " alt=" " style="cursor:pointer; ">
                </div>
            </div>
            <div class="col m12 l6 s12 aniview " data-av-animation="slideInRight ">
                <div class="content card ">
                    <div class="card-content ">
                        <h5>Project Finance(Infrastructure / Non Infrastructure sectors) </h5>
                        <hr>
                        <p>Project finance is the financing of long-term infrastructure, industrial projects and public services based on a non-recourse or limited recourse financial structure, in which project debt and equity used to finance the project
                            are paid back from the cash flow generated by the project. </p>
                        <p>It is a financial service for new projects as well as expansion, diversification and modernization of existing projects in infrastructure and non -infrastructure sectors</p>
                        <br>
                        <h6><i class="material-icons left tiny ">fiber_manual_record</i>Foreign currency term loans</h6>
                        <h6><i class="material-icons left tiny ">fiber_manual_record</i>External Commercial Borrowings</h6>
                        <h6><i class="material-icons left tiny ">fiber_manual_record</i>Export Credit Agency backed long-term debt</h6>
                        <h6><i class="material-icons left tiny ">fiber_manual_record</i>Lines of credit from different multilateral institutions</h6>
                        <h6><i class="material-icons left tiny ">fiber_manual_record</i>Non-fund based facilities like Letter of Credit, Bank Guarantee, Supplier's Credit, Buyer's Credit etc</h6>
                        <h6><i class="material-icons left tiny ">fiber_manual_record</i>Subordinated debt and mezzanine financing.</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
   

    <!--project finance end-->


    <!--term loan start style="filter:#212121; -webkit-filter: grayscale(0.8); filter: grayscale(1);-->

    <div class="row sectionDiv" id="termLoan">
        <!--for large device-->
        <div class="hide-on-med-and-down">
            <div class="parallax-container " style="height:663px;;">
                <div class="parallax">
                    <img class="responsive-img" src="resources/image/termloan1.jpg">
                    <figcaption class="blur ">
                        <h3>Term Loan (TL)</h3>
                        <p>It is a facility given by the bank to any manufacturing or service industry which requires financing to fund the purchase of land, construction of factory building/shed and for the purchase of machinery and equipment. By doing
                            this, the capital expenditure cash flows can be distributed in more</p>
                        <h6> <i class="material-icons left tiny ">fiber_manual_record</i>ROI from 9% onwards </h6>
                        <h6> <i class="material-icons left tiny ">fiber_manual_record</i>As low as 25% Collateral </h6>
                        <h6> <i class="material-icons left tiny ">fiber_manual_record</i>Processing time-45 days</h6>
                        <h6> <i class="material-icons left tiny ">fiber_manual_record</i>7 yrs tenure available</h6>
                    </figcaption>
                </div>
            </div>
        </div>
        <!--for small device-->
        <div class="hide-on-large-only">
            <div class="col s12 m12 l12 ">
                <img class="responsive-img " src="resources/image/termloan1.jpg ">
                <figcaption class="blur ">
                    <h3>Term Loan (TL)</h3>
                    <p>It is a facility given by the bank to any manufacturing or service industry which requires financing to fund the purchase of land, construction of factory building/shed and for the purchase of machinery and equipment. By doing this,
                        the capital expenditure cash flows can be distributed in more</p>
                    <h6> <i class="material-icons left tiny ">fiber_manual_record</i>ROI from 9% onwards </h6>
                    <h6> <i class="material-icons left tiny ">fiber_manual_record</i>As low as 25% Collateral </h6>
                    <h6> <i class="material-icons left tiny ">fiber_manual_record</i>Processing time-45 days</h6>
                    <h6> <i class="material-icons left tiny ">fiber_manual_record</i>7 yrs tenure available</h6>
                </figcaption> <br>
            </div>
        </div>
    </div>
   

    <!--term loan end-->




    <!--purchase and  sales bill discounting start-->
    <div class="row sectionDiv" id="purchaseBillDiscounting">
        <!--for large device-->
        <div class="hide-on-med-and-down ">
            <br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down ">
            <div class="col s12 m12 l6 aniview " data-av-animation="slideInLeft ">
                <div class="zoomin frame ">
                    <img src="resources/image/ExportBillDiscounting.jpg " alt=" " style="cursor:pointer; ">
                </div>
            </div>
            <div class="col m12 l6 s12 aniview " data-av-animation="slideInRight ">
                <div class="content card ">
                    <div class="card-content ">
                        <br>
                        <h5>Purchase & Sales Bill Discounting</h5>
                        <hr>
                        <br>
                        <p>The bill drawn by borrower on his (borrower's) customer and pay him immediately deducting some amount as discount/commission.The Bank then presents the Bill to the borrower's customer on the due date of the Bill and collects the
                            total amount. If the bill is delayed, the borrower or his customer pays the Bank a pre- determined interest depending upon the terms of transaction.
                        </p>
                        <br>
                        <br>

                        <ul>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Customer places the order with Client </li>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Client obtains a prepayment limit </li>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Copies of Invoices, along with a Notice to Pay, are submitted</li>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Acknowledgment from the drawee that he has received the goods in good condition</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--for small device-->
        <div class="hide-on-large-only">
            <div class="col s12 m12 l6 aniview " data-av-animation="slideInLeft ">
                <div class="zoomin frame ">
                    <img src="resources/image/ExportBillDiscounting.jpg " alt=" " style="cursor:pointer; ">
                </div>
            </div>
            <div class="col m12 l6 s12 aniview " data-av-animation="slideInRight ">
                <div class="content card ">
                    <div class="card-content ">
                        <br>
                        <h5>Purchase & Sales Bill Discounting</h5>
                        <hr>
                        <br>
                        <p>The bill drawn by borrower on his (borrower's) customer and pay him immediately deducting some amount as discount/commission.The Bank then presents the Bill to the borrower's customer on the due date of the Bill and collects the
                            total amount. If the bill is delayed, the borrower or his customer pays the Bank a pre- determined interest depending upon the terms of transaction.
                        </p>
                        <br>
                        <br>

                        <ul>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Customer places the order with Client </li>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Client obtains a prepayment limit </li>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Copies of Invoices, along with a Notice to Pay, are submitted</li>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Acknowledgment from the drawee that he has received the goods in good condition</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    <!--purchase and  sales bill discounting end-->

    <!--buyers credit and export bill discounting start-->
    <div class="row sectionDiv" id="exportbillDiscounting">
        <!--for large device-->
        <div class="hide-on-med-and-down">
            <br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down ">
            <div class="col s12 m6 l6 left aniview " data-av-animation="slideInLeft " style="padding-left:0; ">
                <figure class="snip1482 " style="margin-left:1%; ">
                    <figcaption class="center">
                        <h6>Buyers Credit</h6>
                        <hr>
                        <p>It is a short term credit available to an importer (buyer) from overseas lenders such as banks and other financial institution for goods they are importing. The overseas banks usually lend the importer (buyer) based on the letter
                            of comfort (a bank guarantee) issued by the importer's bank.<br>  </p>
                            <ul class="left-align">
                                <li><i class="material-icons left tiny ">fiber_manual_record</i>Rate from Libor + 20 bps onwards.</li>
                                <li><i class="material-icons left tiny ">fiber_manual_record</i>Quick Processing </li>
                                <li><i class="material-icons left tiny ">fiber_manual_record</i>Well - organized Execution </li>
                            </ul>
                      
                    </figcaption>
                    <img class="responsive-img " src="resources/image/buyersCredit2.jpg " alt="sample45 " />
                </figure>
            </div>
            <div class="col s12 m6 l6 right aniview " data-av-animation="slideInRight " style="padding-left:0; ">
                <figure class="snip1482 " style="margin-left:1%; ">
                    <figcaption class="center">
                        <h6>Export Bill Discounting (without recourse)</h6>
                        <hr>
                        <p class="flow-text ">A short-term post-shipment financing facility that operates on a "without recourse" basis, whereby the Bank discounts the exporter's usance export documents drawn under Documentary Credit after acceptance of the documents by the
                            Issuing Bank. </p>
                        <br>
                        <ul class="left-align">
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>LIBOR + 200bps onwards</li>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Limit upto 40 Cr.</li>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Limit setup - 2 Weeks</li>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Processing time - 2 - 3 days</li>
                        </ul>

                    </figcaption>
                    <img class="responsive-img " src="resources/image/billDiscount.jpg" alt="sample45 " />
                </figure>
            </div>
        </div>
        <!--for small device-->
        <div class="hide-on-large-only ">
            <div class="col s12 m9 offset-m3 l6 aniview " data-av-animation="slideInLeft ">
                <br><br><br><br>
                <figure class="snip1482 ">
                    <figcaption class="center">
                        <h6>Buyers Credit</h6>
                        <hr>
                        <p>It is a short term credit available to an importer (buyer) from overseas lenders such as banks and
                            <button class="btn-flat white-text " style="font-size:0.8em " id="buyerCredit">Read more</button>
                        </p>
                    </figcaption>
                    <img src="resources/image/buyersCredit2.jpg " alt="sample45 " />
                </figure>
                <figure class="snip1482 " id="divbuyerCredit">
                    <figcaption style="width:100% ">
                        <p class="center ">
                            other financial institution for goods they are importing. The overseas banks usually lend the importer (buyer) based on the letter of comfort (a bank guarantee) issued by the importer's bank.</p>
                            <ul class="left-align">
                                <li><i class="material-icons left tiny ">fiber_manual_record</i>Rate from Libor + 20 bps onwards</li>
                                <li>Quick Processing <i class="material-icons left tiny ">fiber_manual_record</i></li>
                                <li>Well - organized Execution <i class="material-icons left tiny ">fiber_manual_record</i> </li>
                            </ul>
                        
                    </figcaption>
                </figure>

            </div>
            <div class="col s12 m9 offset-m3 l6 aniview " data-av-animation="slideInRight ">
                <figure class="snip1482 ">
                    <figcaption class="center">
                        <h6>Export Bill Discounting (without recourse)</h6>
                        <hr>
                        <p class="flow-text ">A short-term post-shipment financing facility that operates on a "without recourse" basis, </p>
                        <button class="btn-flat white-text " style="font-size:0.8em " id="exportBill">Read more</button>
                    </figcaption>
                    <img src="resources/image/billDiscount.jpg" alt="sample45 " />
                </figure>
                <figure class="snip1482 " id="divexportBill" style="margin-top:0 ">
                    <figcaption style="width:100% ">
                        <p class="flow-text center "> whereby the Bank discounts the exporter's usance export documents drawn under Documentary Credit after acceptance of the documents by the Issuing Bank.
                           </p>  <ul class="left-align">
                                <li><i class="material-icons left tiny">fiber_manual_record</i> LIBOR + 200bps onwards</li>
                                <li><i class="material-icons left tiny ">fiber_manual_record</i>Limit upto 40 Cr.</li>
                                <li><i class="material-icons left tiny">fiber_manual_record</i>Limit setup - 2 Weeks</li>
                                <li><i class="material-icons left tiny ">fiber_manual_record</i>Processing time - 2 - 3 days</li>
                            </ul>

                       
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>
   
    <!--buyers credit and export bill discounting end-->

    <!--overdraft start-->
    <div class="row sectionDiv" id="overDraft">
        <!--for large device-->
        <div class="hide-on-med-and-down">
            <div class="parallax-container " style="height:663px;">
                <div class="parallax"><img class="responsive-img" src="resources/image/draft.jpg">
                    <figcaption class="blur">
                        <h3>Overdraft (OD)</h3>
                        <p>It is a facility given by the bank to companies, to withdraw money "more" than the balance available in their respective accounts. It is granted against assets like financial instrument and property.
                        </p>
                        <h6><i class="material-icons left tiny">fiber_manual_record</i> ROI from 9% onwards </h6>
                        <h6><i class="material-icons left tiny">fiber_manual_record</i> As low as 25% Collateral </h6>
                        <h6><i class="material-icons left tiny">fiber_manual_record</i> Processing time-45 days</h6>
                    </figcaption>
                </div>
            </div>
        </div>
        <!--for small device-->
        <div class="hide-on-large-only">
            <div class="col s12 m12 l12">
                <img class="responsive-img" src="resources/image/draft.jpg">
                <figcaption class="blur">
                    <h3>Overdraft (OD)</h3>
                    <p>It is a facility given by the bank to companies, to withdraw money "more" than the balance available in their respective accounts. It is granted against assets like financial instrument and property.
                    </p>
                    <h6><i class="material-icons left tiny">fiber_manual_record</i> ROI from 9% onwards </h6>
                    <h6><i class="material-icons left tiny">fiber_manual_record</i> As low as 25% Collateral </h6>
                    <h6><i class="material-icons left tiny">fiber_manual_record</i> Processing time-45 days</h6>
                </figcaption>
            </div>
        </div>
    </div>
    <!--<div class="row hide-on-large-only " id="overDraft">
        <div class="col s12 m12 l12">
            <img class="responsive-img" src="resources/image/finalpics/draft.jpg">
            <figcaption class="blur">
                <h3>Overdraft (OD)</h3>
                <p>it is a facility given by the bank to companies, to withdraw money "more" than the balance available in their respective accounts. It is granted against assets like financial instrument and property.
                </p>
                <h6><i class="material-icons left tiny">fiber_manual_record</i> ROI from 9% onwards </h6>
                <h6><i class="material-icons left tiny">fiber_manual_record</i> As low as 25% Collateral </h6>
                <h6><i class="material-icons left tiny">fiber_manual_record</i> Processing time – 45 days</h6>
            </figcaption>
        </div>
    </div>-->

    <!--overdraft end-->
    <!--Export Packing Credit start-->
    <div class="row sectionDiv" id="exportpacking">
        <!--for large device-->
        <div class="hide-on-med-and-down ">
            <br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down ">
            <div class="col s12 m12 l6 aniview " data-av-animation="slideInLeft ">
                <div class="zoomin frame ">
                    <img src="resources/image/preShipment.jpg " alt=" " style="cursor:pointer; ">
                </div>

            </div>
            <div class="col m12 l6 s12 aniview " data-av-animation="slideInRight ">
                <div class="content card ">
                    <div class="card-content ">
                        <h5> Export Packing Credit</h5>
                        <hr>
                        <h6><b>Pre-shipment Credit (Packing Credit)</b></h6>
                        <p>Pre Shipment credit is issued by a financial institution when the seller wants the payment of the goods before shipment. The main objectives behind pre-shipment credit or pre export finance are to enable exporter to: Procure raw
                            materials. Carry out manufacturing process </p>
                        <h6><b> Post-Shipment Credit</b></h6>
                        <p>it allows any loan or advance granted or any other credit provided by a bank to an exporter of goods or (and) services from India from the date of extending credit after shipment of goods or (and) rendering of services to the date
                            of realization of export proceeds as per the period of realization prescribed by FED, and includes any loan or advance granted to an exporter, in consideration of, or on the security of any duty drawback allowed by the Government
                            from time to time. Banks serves with low interest rate to exporters under post shipment credit based on the guidelines of Reserve Bank.</p>
                    </div>
                </div>
            </div>
        </div>
        <!--for small device-->
        <div class="hide-on-large-only">
            <div class="col s12 m12 l6 aniview " data-av-animation="slideInLeft ">
                <div class="zoomin frame ">
                    <img src="resources/image/preShipment.jpg " alt=" " style="cursor:pointer; ">
                </div>

            </div>
            <div class="col m12 l6 s12 aniview " data-av-animation="slideInRight ">
                <div class="content card ">
                    <div class="card-content ">
                        <h5> Export Packing Credit</h5>
                        <hr>
                        <h6><b>Pre-shipment Credit (Packing Credit)</b></h6>
                        <p>Pre Shipment credit is issued by a financial institution when the seller wants the payment of the goods before shipment. The main objectives behind pre-shipment credit or pre export finance are to enable exporter to: Procure raw
                            materials. Carry out manufacturing process </p>
                        <h6><b> Post-Shipment Credit</b></h6>
                        <p>it allows any loan or advance granted or any other credit provided by a bank to an exporter of goods or (and) services from India from the date of extending credit after shipment of goods or (and) rendering of services to the date
                            of realization of export proceeds as per the period of realization prescribed by FED, and includes any loan or advance granted to an exporter, in consideration of, or on the security of any duty drawback allowed by the Government
                            from time to time. Banks serves with low interest rate to exporters under post shipment credit based on the guidelines of Reserve Bank.</p>


                    </div>
                </div>


            </div>
        </div>
    </div>
    
    <!--Export Packing Credit end-->





    <!--non fund to fund and bank guarantees start-->
    <div class="row sectionDiv" id="nonfund">
        <!--for large device-->
        <div class="hide-on-med-and-down ">
            <br><br>
            <br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down ">
            <div class="col s12 m6 l6 aniview " data-av-animation="slideInLeft " style="padding-left:0; ">
                <figure class="snip1482 " style="margin-left:1%; ">
                    <figcaption class="center">
                        <h6>Non Fund to Fund</h6>
                        <hr>

                        <p> We facilitate customer to get maximum output from their business by supporting them to convert non fund based limit (LC / BG) to fund based limit.
                        </p> <br><br><br><br><br><br><br><br><br><br>
                    </figcaption>
                    <img class="responsive-img " src="resources/image/nonFuntoFund.jpg " alt="sample45 " />
                </figure>
            </div>
            <div class="col s12 m6 l6 hide-on-med-and-down right aniview " data-av-animation="slideInRight " style="padding-left:0; ">
                <figure class="snip1482 " style="margin-left:1%; ">
                    <figcaption class="center">
                        <h6>Bank Guarantees</h6>
                        <hr>
                        <p class="flow-text ">It is a commitment from a bank that the liabilities of a debtor will be met in the event that you fail to fulfill your contractual obligations. We are providing Bank Guarantees which will be available to you against minimal requirements
                            and in the shortest possible time</p>
                        <br>
                        <ul class="left-align">
                            <li>Performance Guarantee<i class="material-icons left tiny ">fiber_manual_record</i></li>
                            <li>Bid Bond Guarantee<i class="material-icons left tiny ">fiber_manual_record</i></li>
                            <li>Financial Guarantee<i class="material-icons left tiny ">fiber_manual_record</i></li>
                            <li>Foreign Bank Guarantee<i class="material-icons left tiny ">fiber_manual_record</i></li>
                            <li>Deferred Payment Guarantee<i class="material-icons left tiny ">fiber_manual_record</i></li>
                        </ul>
                    </figcaption>
                    <img class="responsive-img " src="resources/image/bank.jpg " alt="sample45 " />
                </figure>
            </div>
        </div>
        <!--for small device-->
        <div class="hide-on-large-only">
            <div class="col s12 m9 offset-m3 l6 aniview " data-av-animation="slideInLeft ">
                <br><br><br><br>
                <figure class="snip1482 ">
                    <figcaption class="center">
                        <h6>Non Fund to Fund</h6>
                        <hr>
                        <p> We facilitate customer to get maximum output from their business by supporting them to convert non fund based limit (LC / BG) to fund based limit.

                        </p>

                    </figcaption>
                    <img src="resources/image/nonFuntoFund.jpg " alt="sample45 " />
                </figure>
            </div>
            <div class="col s12 m9 offset-m3 l6 hide-on-large-only aniview " data-av-animation="slideInRight ">
                <figure class="snip1482 ">
                    <figcaption  class="center">
                        <h6>Bank Guarantees</h6>
                        <hr>
                        <p>It is a commitment from a bank that the liabilities of a debtor will be met in the event that you fail to fulfill your contractual obligations.
                            <button class="btn-flat white-text " style="font-size:0.8em " id="bankGuarantee">Read more</button>
                        </p>
                    </figcaption>
                    <img src="resources/image/bank.jpg " alt="sample45 " />
                </figure>
                <figure class="snip1482 " id="divbankGuarantee">
                    <figcaption style="width:100% ">
                        <p class="center ">
                            We are providing Bank Guarantees which will be available to you against minimal requirements and in the shortest possible time
                           </p>  <ul class="left-align">
                                <li>Performance Guarantee <i class="material-icons left tiny ">fiber_manual_record</i></li>
                                <li>Bid Bond Guarantee <i class="material-icons left tiny ">fiber_manual_record</i></li>
                                <li>Financial Guarantee <i class="material-icons left tiny ">fiber_manual_record</i></li>
                                <li>Foreign Bank Guarantee <i class="material-icons left tiny ">fiber_manual_record</i></li>
                                <li>Deferred Payment Guarantee <i class="material-icons left tiny ">fiber_manual_record</i></li>
                            </ul>
                       
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>
   
    <!--non fund to fund and bank guarantees end-->
    <!--CORPORATE FINANCE end-->

    <!--Retail finance start-->
    <!--home loan start -->
    <div class="row sectionDiv" id="homeLoan">
        <br>
        <!--for large device-->
        <div class="hide-on-med-and-down">
            <div class="parallax-container" style="height:663px;">

                <div class="parallax "><img class="responsive-img" src="resources/image/HousingLoan.jpg">
                    <figcaption class="blur">
                        <h3>Home Loan</h3>
                        <p>We offer home loan at higher eligibility and lower EMIs at attractive interest rates.
                        </p>
                        <h6><i class="material-icons left tiny">fiber_manual_record</i>ROI 8.35% onwards.</h6>
                        <h6><i class="material-icons left tiny">fiber_manual_record</i>Processing time-15 days</h6>
                        <h6><i class="material-icons left tiny">fiber_manual_record</i>Documentation and quick approval </h6>
                        <h6><i class="material-icons left tiny">fiber_manual_record</i>Balance transfer + top up</h6>
                        <h6><i class="material-icons left tiny">fiber_manual_record</i>No prepayment charges</h6>
                        <h6><i class="material-icons left tiny">fiber_manual_record</i>Transparent Processing</h6>
                    </figcaption>
                </div>
            </div>
        </div>
        <!--for small device-->
        <div class="hide-on-large-only">
            <img class="responsive-img" src="resources/image/HousingLoan.jpg" style="position:relative;">
            <figcaption class="blur">
                <h3>Home Loan</h3>
                <p>We offer home loan at higher eligibility and lower EMIs at attractive interest rates.
                </p>
                <h6><i class="material-icons left tiny">fiber_manual_record</i>ROI 8.35% onwards.</h6>
                <h6><i class="material-icons left tiny">fiber_manual_record</i>Processing time - 15 days</h6>
                <h6><i class="material-icons left tiny">fiber_manual_record</i>Documentation and quick approval </h6>
                <h6><i class="material-icons left tiny">fiber_manual_record</i>Balance transfer + top up</h6>
                <h6><i class="material-icons left tiny">fiber_manual_record</i>No prepayment charges</h6>
                <h6><i class="material-icons left tiny">fiber_manual_record</i>Transparent Processing</h6>
            </figcaption>
        </div>
    </div>
   
    <!--home loan end-->

    <!--personal loan and unsecuredBusinessLoan start-->
    <div class="row sectionDiv" id="personalLoan">

        <br><br><br><br class="hide-on-med-and-down"><br class="hide-on-med-and-down"><br class="hide-on-med-and-down">
        <!--for large device-->
        <div class="hide-on-med-and-down">
            <br><br>
            <div class="col s12 m6 l6  left aniview" data-av-animation="slideInLeft" style="padding-left:0;">
                <figure class="snip1482" style="margin-left:1%;">
                    <figcaption class="center">
                        <h6>Personal Loan</h6>
                        <hr>
                        <p>We offer personal loan at higher eligibility and lower EMIs at attractive interest rates. <br><br> <br><br>
                        </p>
                       
                        <ul class="left-align">
                            <li>ROI 10.99% onwards.<i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Processing time - 5 days<i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Tenure upto 5 years<i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Balance transfer + top up<i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Transparent Processing<i class="material-icons left tiny">fiber_manual_record</i></li>
                        </ul>
                        
                    </figcaption>
                    <img class="responsive-img" src="resources/image/personalLoan.jpg">
                </figure>
            </div>
            <div class="col s12 m6 l6  right aniview" data-av-animation="slideInRight" style="padding-left:0;">
                <figure class="snip1482" style="margin-left:1%;">
                    <figcaption class="center">
                        <h6>Unsecured Business Loan</h6>
                        <hr>
                        <p class="flow-text"> We offers collateral-free EMI based loan to a business. Avail this facility for business purpose and enjoy the following benefits of the service. </p>
                        <br>
                        <ul class="left-align">
                            <li>ROI 14% onwards.<i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Processing time - 5 days<i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Tenure upto 5 years<i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Documentation and quick approval<i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Balance transfer + top up<i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Transparent Processing<i class="material-icons left tiny">fiber_manual_record</i></li>
                        </ul>
                    </figcaption>
                    <img class="responsive-img" src="resources/image/businessLoan.jpg" alt="sample45" />
                </figure>
            </div>
        </div>
        <!--for small device-->
        <div class="hide-on-large-only">
            <br>
            <div class="col s12 m9 l6   offset-m3 aniview" data-av-animation="slideInLeft">
                <figure class="snip1482">
                    <figcaption class="center">
                        <h6>Personal Loan</h6>
                        <hr>
                        <p>We offer personal loan at higher eligibility and lower EMIs at attractive interest rates.
                        </p>
                        <button class="btn-flat white-text" style="font-size:0.8em" id="loanAgainstprop">Read more</button>
                    </figcaption>

                    <img class="responsive-img" src="resources/image/personalLoan.jpg">
                </figure>

                <figure class="snip1482" id="divloanAgainstprop">
                    <figcaption style="width:100%" class="center">

                        <ul class="left-align">
                            <li>ROI 10.99% onwards.<i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Processing time - 5 days<i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Tenure upto 5 years<i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Balance transfer + top up<i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Transparent Processing<i class="material-icons left tiny">fiber_manual_record</i></li>
                        </ul>
                    </figcaption>

                </figure>
            </div>
            <div class="col s12 m9 l6  offset-m3 aniview" data-av-animation="slideInRight">
                <figure class="snip1482">
                    <figcaption class="center">
                        <h6>Unsecured Business Loan</h6>
                        <hr>
                        <p class="flow-text">We offers collateral-free EMI based loan to a business. </p>

                        <button class="btn-flat white-text " style="font-size:0.8em" id="unsecuredBusinessLoan">Read more</button>
                    </figcaption>
                    <img src="resources/image/businessLoan.jpg" alt="sample45" />
                </figure>
                <figure class="snip1482" id="divunsecuredBusinessLoan" style="margin-top:0">
                    <figcaption style="width:100%" class="center">
                        <p>Avail this facility for business purpose and enjoy the following benefits of the service.</p>
                        <ul class="left-align">
                            <li><i class="material-icons left tiny">fiber_manual_record</i>ROI 14% onwards.</li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>Processing time - 5 days</li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>Tenure upto 5 years</li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>Documentation and quick approval</li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>Balance transfer + top up</li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>Transparent Processing</li>
                        </ul>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>



      <!--personal loan and unsecuredBusinessLoan end-->


    <!-- loanAgainstProperty start-->

    <div class="row sectionDiv" id="loanAgainstProp">
        <!--for large device-->
        <div class="hide-on-med-and-down">
            <div class="parallax-container " style="height:660px;">
                <div class="parallax"><img class="responsive-img" src="resources/image/loanAgainstProperty.jpg" alt="sample45" />
                    <figcaption class="blur">

                        <h3>Loan against Property</h3>

                        <p>Mortgaging existing property is a great way to get a loan at low interest rates to meet your current financial needs without actually selling off the same.</p>

                        <h6><i class="material-icons left tiny">fiber_manual_record</i>ROI 9.5% onwards.</h6>
                        <h6><i class="material-icons left tiny">fiber_manual_record</i>Processing time - 15 days</h6>
                        <h6><i class="material-icons left tiny">fiber_manual_record</i>Tenure upto 15 years</h6>
                        <h6><i class="material-icons left tiny">fiber_manual_record</i>Documentation and quick approval</h6>
                        <h6><i class="material-icons left tiny">fiber_manual_record</i>Balance transfer + top up</h6>
                        <h6><i class="material-icons left tiny">fiber_manual_record</i>Transparent Processing</h6>

                    </figcaption>
                </div>
            </div>
        </div>
        <!--for small device-->
        <div class="hide-on-large-only">
            <div class="col s12 m12 l12">
                <img class="responsive-img" src="resources/image/loanAgainstProperty.jpg">
                <figcaption class="blur">
                    <h3>Loan against Property</h3>
                    <p>Mortgaging existing property is a great way to get a loan at low interest rates to meet your current financial needs without actually selling off the same.</p>
                    <h6><i class="material-icons left tiny">fiber_manual_record</i>ROI 9.5% onwards.</h6>
                    <h6><i class="material-icons left tiny">fiber_manual_record</i>Processing time - 15 days</h6>
                    <h6><i class="material-icons left tiny">fiber_manual_record</i>Tenure upto 15 years</h6>
                    <h6><i class="material-icons left tiny">fiber_manual_record</i>Documentation and quick approval</h6>
                    <h6><i class="material-icons left tiny">fiber_manual_record</i>Balance transfer + top up</h6>
                    <h6><i class="material-icons left tiny">fiber_manual_record</i>Transparent Processing</h6>

                </figcaption>
            </div>
        </div>
    </div>
   
    <!--loanAgainstProperty end-->


    <!--leaseRentalDiscount and loanAgainstSecurity START-->
    <div class="row sectionDiv" id="leaseRentalDiscount">
        <br><br><br> <br><br class="hide-on-med-and-down"><br class="hide-on-med-and-down"><br class="hide-on-med-and-down">
        <!--for large device-->
        <div class="hide-on-med-and-down">
            <div class="col s12 m6 l6  left aniview" data-av-animation="slideInLeft" style="padding-left:0;">
                <figure class="snip1482" style="margin-left:1%;">
                    <figcaption class="center">
                        <h6>Lease Rental Discounting</h6>
                        <hr>

                        <p>Lease Rental Discounting is a term loan offered against rental receipts derived from lease contracts with corporate tenants, which provides access to capital for Business Banking Clients
                        </p> <br>
                        <br> <br>
                        <ul class="left-align">
                            <li>ROI 10%.<i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Processing time - 15 days <i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Tenure upto 10 years <i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Balance transfer + top up <i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Transparent Processing <i class="material-icons left tiny">fiber_manual_record</i></li>
                        </ul>

                    </figcaption>
                    <img class="responsive-img" src="resources/image/Leaserentdiscounting.jpg" alt="sample45" />
                </figure>
            </div>
            <div class="col s12 m6 l6  right aniview" data-av-animation="slideInRight" style="padding-left:0;">
                <figure class="snip1482" style="margin-left:1%;">
                    <figcaption class="center">
                        <h6>Loan Against Securities</h6>
                        <hr>
                        <p class="flow-text"> This facility is available against the following securities, Demat shares, Mutual Fund units, Fixed Maturity plans (FMP), Exchange Traded Funds (ETF), Insurance Policies, Savings Bonds. </p>
                        <br>
                        <ul class="left-align">
                            <li>ROI 11% .<i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Processing time - 5 days <i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Loan amount upto 10 crores.<i class="material-icons left tiny">fiber_manual_record</i> </li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>Interest charged only on the amount utilized </li>
                            <li>Nil foreclosure charges <i class="material-icons left tiny">fiber_manual_record</i></li>
                            <li>Security Swap <i class="material-icons left tiny">fiber_manual_record</i></li>
                        </ul>
                    </figcaption>
                    <img class="responsive-img" src="resources/image/loanAgainstSecurity1.jpg" alt="sample45" />
                </figure>
            </div>
        </div>

        <!--for small device-->
        <div class="hide-on-large-only">
            <div class="col s12 m9 l6  offset-m3 aniview" data-av-animation="slideInLeft">
                <figure class="snip1482">
                    <figcaption class="center">
                        <h6>Lease Rental Discounting</h6>
                        <hr>
                        <p>Lease Rental Discounting is a term loan offered against rental receipts derived from </p>
                        <button class="btn-flat white-text" style="font-size:0.8em" id="leaseRentalDiscountbtn">Read more</button>

                    </figcaption>
                    <img src="resources/image/Leaserentdiscounting.jpg" alt="sample45" />
                </figure>
                <figure class="snip1482" id="divleaseRentalDiscount">
                    <figcaption style="width:100%" class="center">
                        <p class="center">lease contracts with corporate tenants,which provides access to capital for Business Banking Clients</p>

                        <ul class="left-align">
                            <li><i class="material-icons left tiny">fiber_manual_record</i>ROI 10%.</li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>Processing time - 15 days</li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>Tenure upto 10 years</li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>Balance transfer + top up</li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>Transparent Processing</li>
                        </ul>

                    </figcaption>
                    <!--<img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/sample45.jpg" alt="sample45" />-->
                </figure>
            </div>
            <div class="col s12 m9 l6  offset-m3 aniview" data-av-animation="slideInRight">
                <figure class="snip1482">
                    <figcaption class="center">
                        <h6>Loan Against Securities</h6>
                        <hr>
                        <p class="flow-text">This facility is available against the following securities, Demat shares, Mutual Fund units, </p>

                        <button class="btn-flat white-text " style="font-size:0.8em" id="loanAgainstSecurity">Read more</button>
                    </figcaption>
                    <img src="resources/image/loanAgainstSecurity1.jpg" alt="sample45" />
                </figure>
                <figure class="snip1482" id="divloanAgainstSecurity" style="margin-top:0">
                    <figcaption style="width:100%" >
                        <p class="center">Fixed Maturity plans (FMP), Exchange Traded Funds (ETF), Insurance Policies, Savings Bonds.</p>
                        <ul class="left-align">
                            <li><i class="material-icons left tiny">fiber_manual_record</i>ROI 11% .</li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>Processing time - 5 days</li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>Loan amount upto 10 crores.</li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>Interest charged only on the amount utilized</li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>Nil foreclosure charges</li>
                            <li><i class="material-icons left tiny">fiber_manual_record</i>Security Swap</li>
                        </ul>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>
   
    <!--leaseRentalDiscount and loanAgainstSecurity end-->

    <!--Retail finance end-->


    <!--other services start-->
    <div class="row sectionDiv">
        <div class="col s12 m12 l12 hide-on-med-and-down ">
            <br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down ">
            <div class="col s12 m12 l6 aniview " data-av-animation="slideInLeft ">
                <div class="zoomin frame ">
                    <img src="resources/image/otherServices.jpg " alt=" " style="cursor:pointer; ">
                </div>
            </div>
            <div class="col m12 l6 s12 aniview " data-av-animation="slideInRight ">

                <div class="content card ">
                    <div class="card-content ">
                        <br>
                        <h5>Other Services</h5>
                        <hr>
                        <br>
                        <ul>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Specialized in Construction / Real Estate Funding. </li>
                            <br>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Multiple Banking</li>
                            <br>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Inter Corporate Deposits.</li><br>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Pari Pasu</li><br>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Corporate Debt</li>
                        </ul>

                        <br> <br>
                    </div>
                </div>

            </div>
        </div>
        <div class="col s12 m12 l12 hide-on-large-only ">
            <br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down "><br class="hide-on-med-and-down ">
            <div class="col s12 m12 l6 aniview " data-av-animation="slideInLeft ">
                <div class="zoomin frame ">
                    <img src="resources/image/otherServices.jpg " alt=" " style="cursor:pointer; ">
                </div>
            </div>
            <div class="col m12 l6 s12 aniview " data-av-animation="slideInRight ">

                <div class="content card ">
                    <div class="card-content ">
                        <br>
                        <h5>Other Services</h5>
                        <hr>
                        <br>
                        <ul>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Specialized in Construction / Real Estate Funding. </li>
                            <br>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Multiple Banking</li>
                            <br>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Inter Corporate Deposits.</li><br>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Pari Pasu</li><br>
                            <li><i class="material-icons left tiny ">fiber_manual_record</i>Corporate Debt</li>
                        </ul>

                        <br> <br> <br> <br>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--other services end-->

    <!--OUR APPROACH start-->

    <!--our approach and footer for small device-->
    <div class="row ourApproach sectionDiv">
        <div class="col s12 m12 l12 " style="padding:0 0; margin-top:4%;">
            <div class="parallax-container " style="background-color: rgba(0, 0, 0, 0.6); ">
                <div class="parallax "><img src="resources/image/ourAapproach1.jpg " style="webkit-filter: grayscale(1);filter: grayscale(1%);top:-400px; "></div>
                <div class="row ">
						                    <h4 class="center white-text ">Our Approach</h4>
                    <div class="col s6 l4 m6 center aniview " data-av-animation="zoomIn ">
                        <img class="responsive-img " src="resources/image/serviceOur2.png " alt="Client Requirement ">
                        <!--<i class="material-icons medium ">record_voice_over</i>-->
                        <p class="white-text ">Understanding client requirement</p>
                    </div>
                    <div class="col s6 l4 m6 center aniview " data-av-animation="zoomIn ">
                        <img class="responsive-img " src="resources/image/serviceOur3.png " alt="Debt Funding ">
                        <!--<i class="material-icons medium ">person_add</i>-->
                        <p class="white-text ">Deciding most effective debt funding strategy</p>
                    </div>
                    <div class="col s6 l4 m6 center aniview " data-av-animation="zoomIn ">
                        <img class="responsive-img " src="resources/image/serviceOur4.png " alt="discussing deals ">
                        <!--<i class="material-icons medium ">people</i>-->
                        <p class="white-text ">Approaching & discussing deal with potential lenders</p>
                    </div>
                    <div class="col s6 l4 m6 center aniview " data-av-animation="zoomIn ">
                        <img class="responsive-img " src="resources/image/serviceOur5.png " alt="Short listing lenders ">
                        <!--<i class="material-icons medium ">work</i>-->
                        <p class="white-text ">Short listing lenders on predetermined parameters</p>
                    </div>
                    <div class="col s6 l4 m6 center aniview " data-av-animation="zoomIn ">
                        <img class="responsive-img " src="resources/image/serviceOur6.png " alt="negotiations ">
                        <!--<i class="material-icons medium ">insert_drive_file</i>-->
                        <p class="white-text ">Analyze offers & drive negotiations</p>

                    </div>
                    <div class="col s6 l4 m6 center aniview " data-av-animation="zoomIn ">
                        <img class="responsive-img " src="resources/image/serviceOur1.png " alt="Core Activities ">
                        <!--<i class="material-icons medium center-align ">access_time</i>-->
                        <p class="white-text ">Identifying the core activities to be strengthened and non-core activities to be spun- off/sold.</p>

                    </div>
                    <br><br><br>
                </div>
            </div>
        </div>
        <!--footer start-->
        <div class="col s12 m12 l12 " style="padding:0 0;">
            	<%@include file="components/footer.jsp"%>
        </div>
    </div>

    <!--footer end-->

    <!--action button strat-->
    <div class="fixed-action-btn ">
        <a class="btn-floating btn-large black ">
            <i class="large material-icons ">view_list</i>
        </a>
        <ul>
            <li><a class="btn-floating red tooltipped " data-position="left " data-delay="50 " data-tooltip="Emi Calculator " href="emiPageForWebsite"><i class="material-icons ">insert_chart</i></a></li>
            <li><a class="btn-floating yellow darken-1 tooltipped " data-position="left " data-delay="50 " data-tooltip="" href="contactPageForWebsite"><i class="material-icons ">perm_contact_calendar</i></a></li>
            <li><a id="modal" class="btn-floating green modal-trigger tooltipped " data-position="left " data-delay="50 " data-tooltip="Call Me " href="#contactForm"><i class="material-icons ">phone</i></a></li>

        </ul>
    </div>




    <!-- Modal Structure addingUserDetailsForWebsiteServicesPageModal-->
          <div id="contactForm" class="modal modal-fixed-footer grey lighten-3">
     <form method="post" action="addingUserDetailsForWebsiteHomePageModal" name="contactForm">
        <div class="row modal-content ">
            <h4 class="center ">Let Us Contact You</h4>
            <hr>
            
               <div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">people</i>
					 <input id="userDetailsName" name="userDetailsName" type="text" class="validate text"  value="" title="Enter Valid Name">
					 <label for="userDetailsName">Name</label>
				</div>
				
			
			<div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">email</i>
					 <input id="emailId" name="emailId" type="email" class="validate"  value="" title="Enter Valid Email"> 
					 <label for="emailId">Email</label>
				</div>
				
				
				<div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">phone</i>
					 <input id="mobileNumber" name="mobileNumber" type="text" class="validate num"  value="" title="Enter Valid Mobile Number">
					  <label for="mobileNumber">Phone</label>
				</div>
				
				 
				<div class="input-field col s12 m6 l6 offset-l3 offset-m3">
					<i class="material-icons prefix">redeem</i> 
					<select multiple name="serviceId" id="selectService" required  aria-required="true">
						<option value="" disabled>Choose your option</option>
						
						<c:if test="${not empty serviceList}">
						<c:forEach var="service" items="${serviceList}">
						
							<option value="<c:out value="${service.serviceId}" />"><c:out
									value="${service.serviceName}" /></option>
						</c:forEach>
						</c:if>
						
					</select> <label>Select Loan Type</label>
						
				</div>
		
		</div>

		<div class="row modal-footer grey lighten-3">
			<div class="col s6 m6 l6 offset-m1 offset-l1 offset-s3">
				<button type="submit" name="action" id="addUser"
					class="modal-action  btn waves-effect black">Submit</button>
			</div>
		</div>
        </form>
    </div>
    <!--action button end-->

    <!--content end-->
    <!--OUR APPROACH end-->
    <div onclick="topFunction()" id="myBtn"><img src="resources/image/arrow-up-black.png" alt="scroll up" /></div>
</body>
</html>