// Wait for the DOM to be ready
$(function() {
    // Initialize form validation on the registration form.
    // It has the name attribute "registration"
    $("form[name='contact']").validate({
        // Specify validation rules
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side
            name: "required",
            phone: {
                required: true,
                number: true,
                //pattern: [/^[7-9]{1}[0-9]{9}$/]
                minlength: 10,
                maxlength: 10



            },
            email: {
                required: true,
                // Specify that email should be validated
                // by the built-in "email" rule
                email: true
            },
            message: "required"


        },
        // Specify validation error messages
        messages: {
            name: "Please enter your firstname",
            phone: {

                required: "Please provide a mobile number",
                number: "please enter only number",
                // pattern: "please enter a valid mobile no"
                minlength: "mobile no should be 10 digit",
                maxlength: "mobile no should be 10 digit"



            },

            email: "Please enter a valid email address",

            message: "please enter a message"
        },

        errorElement: 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function(form) {
            form.submit();
        }
    });
    $("form[name='contactForm']").validate({
        // Specify validation rules
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined
            // on the right side
            // name: "required",
            mobile: {
                required: true,
                number: true,
                //pattern: [/^[7-9]{1}[0-9]{9}$/]
                minlength: 10,
                maxlength: 10



            },
            email: {
                required: true,
                // Specify that email should be validated
                // by the built-in "email" rule
                email: true
            },
            // message: "required"


        },
        // Specify validation error messages
        messages: {
            //    name: "Please enter your firstname",
            mobile: {
                required: "Please provide a mobile number",
                number: "please enter only number",
                // pattern: "please enter a valid mobile no"
                minlength: "mobile no should be 10 digit",
                maxlength: "mobile no should be 10 digit"
            },

            email: "Please enter a valid email address",

            // message: "please enter a message"
        },

        errorElement: 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function(form) {
            form.submit();
        }
    });
});