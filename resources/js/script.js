 $(document).ready(function() {
     $('.aniview').AniView();
     $(".button-collapse").sideNav();
     $('.modal').modal();
     $('select').material_select();
     $('ul.tabs').tabs();
     $('.slider').slider();
     $(".leftclick").click(function() {
         $('.slider').slider('prev');
     });
     $(".rightclick").click(function() {
         $('.slider').slider('next');
     });
 });
 window.onscroll = function() {
     scrollFunction()
 };

 function scrollFunction() {
     if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
         document.getElementById("myBtn").style.display = "block";
     } else {
         document.getElementById("myBtn").style.display = "none";
     }
 }
 // When the user clicks on the button, scroll to the top of the document
 function topFunction() {
     document.body.scrollTop = 0; // For Chrome, Safari and Opera 
     document.documentElement.scrollTop = 0; // For IE and Firefox
 }